# translation of eel.HEAD.ar.po to Arabic
# translation of eel.HEAD.ar.po to
# translation of eel.po to
# Copyright (C) 2001,2002,2003, 2004, 2006 Free Software Foundation, Inc.
#
# Mohammed Gamal <f2c2001@yahoo.com>, 2001.
# Isam Bayazidi <bayazidi@arabeyes.org>, 2002.
# Arafat Medini <lumina@silverpen.de>, 2003.
# l <lumina@silverpen.de>, 2004.
# Abdulaziz Al-Arfaj <alarfaj0@yahoo.com>, 2004.
# Djihed Afifi <djihed@gmail.com>, 2006.
# Khaled Hosny <khaledhosny@eglug.org>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: eel.HEAD.ar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-10-30 00:51+0000\n"
"PO-Revision-Date: 2006-11-16 12:23+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=4; plural=n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n"

#: ../eel/eel-alert-dialog.c:115
msgid "Image/label border"
msgstr "حدود الصّورة/العلامة"

#: ../eel/eel-alert-dialog.c:116
msgid "Width of border around the label and image in the alert dialog"
msgstr "عرض الحدود حول العلامة و الصّورة في حوار التّنبيه"

#: ../eel/eel-alert-dialog.c:125
msgid "Alert Type"
msgstr "نوع التّنبيه"

#: ../eel/eel-alert-dialog.c:126
msgid "The type of alert"
msgstr "نوع التّنبيه"

#: ../eel/eel-alert-dialog.c:134
msgid "Alert Buttons"
msgstr "أزرار التّنبيه"

#: ../eel/eel-alert-dialog.c:135
msgid "The buttons shown in the alert dialog"
msgstr "الأزرار المعروضة في حوار التّنبيه"

#: ../eel/eel-alert-dialog.c:199
msgid "Show more _details"
msgstr "أظهر _تفاصيل أكثر"

#: ../eel/eel-canvas.c:1222 ../eel/eel-canvas.c:1223
msgid "X"
msgstr "س"

#: ../eel/eel-canvas.c:1229 ../eel/eel-canvas.c:1230
msgid "Y"
msgstr "ص"

#: ../eel/eel-editable-label.c:366
msgid "Text"
msgstr "النّص"

#: ../eel/eel-editable-label.c:367
msgid "The text of the label."
msgstr "نص العلامة."

#: ../eel/eel-editable-label.c:373
msgid "Justification"
msgstr "المحاذاةف"

#: ../eel/eel-editable-label.c:374
msgid ""
"The alignment of the lines in the text of the label relative to each other. "
"This does NOT affect the alignment of the label within its allocation. See "
"GtkMisc::xalign for that."
msgstr ""
"محاذاة السّطور في نص العلامة نسبةً لبعضها البعض. هذا لا يؤثّر على محاذاة "
"العلامة ضمن موقعها. راجع GtkMisc::xalign للمزيد من المعلومات."

#: ../eel/eel-editable-label.c:382
msgid "Line wrap"
msgstr "لف السّطور"

#: ../eel/eel-editable-label.c:383
msgid "If set, wrap lines if the text becomes too wide."
msgstr "في حال تمكينه، فلتلفّ السّطور إذا أصبح النّص عريضاً جدّاً."

#: ../eel/eel-editable-label.c:390
msgid "Cursor Position"
msgstr "موقع المؤشّر"

#: ../eel/eel-editable-label.c:391
msgid "The current position of the insertion cursor in chars."
msgstr "الموقع الحالي لمؤشّر الإدراج في الرّموز."

#: ../eel/eel-editable-label.c:400
msgid "Selection Bound"
msgstr "المنتقى مثبّت"

#: ../eel/eel-editable-label.c:401
msgid "The position of the opposite end of the selection from the cursor in chars."
msgstr "موقع الطّرف المقابل للمنتقى عن المؤشر في الرّموز."

#: ../eel/eel-editable-label.c:3049
msgid "Select All"
msgstr "انتقي الكل"

#: ../eel/eel-editable-label.c:3060
msgid "Input Methods"
msgstr "طرق الإدخال"

#: ../eel/eel-gconf-extensions.c:83
#, c-format
msgid ""
"GConf error:\n"
"  %s"
msgstr ""
"خطأ GConf:\n"
"  %s"

#: ../eel/eel-gconf-extensions.c:87
#, c-format
msgid "GConf error: %s"
msgstr "خطأ GConf: %s"

#: ../eel/eel-gconf-extensions.c:90
msgid "All further errors shown only on terminal."
msgstr "ستعرض جميع الأخطاء القادمة في  الطّرفيّة فقط."

#. localizers: These strings are part of the strftime
#. * self-check code and must be changed to match what strtfime
#. * yields. The first one is "%m/%d/%y, %I:%M %p".
#.
#: ../eel/eel-glib-extensions.c:1193
msgid "01/01/00, 01:00 AM"
msgstr "01/01/00، 01:00 ص"

#. The second one is "%-m/%-d/%-y, %-I:%M %p".
#: ../eel/eel-glib-extensions.c:1195
msgid "1/1/00, 1:00 AM"
msgstr "1/1/00،1:00 ص"

#. The third one is "%_m/%_d/%_y, %_I:%M %p".
#: ../eel/eel-glib-extensions.c:1197
msgid " 1/ 1/00,  1:00 AM"
msgstr " 1/ 1/00،  1:00 ص"

#: ../eel/eel-gnome-extensions.c:284
msgid "No image was selected."
msgstr "لم تنتقى أيّة صورة."

#: ../eel/eel-gnome-extensions.c:285
msgid "You must click on an image to select it."
msgstr "عليك أن تنقر على صورةٍ لانتقائها."

#: ../eel/eel-mime-application-chooser.c:229
msgid "Default"
msgstr "الافتراضي"

#: ../eel/eel-mime-application-chooser.c:237
msgid "Icon"
msgstr "أيقونة"

#: ../eel/eel-mime-application-chooser.c:246
msgid "Name"
msgstr "الإسم"

#: ../eel/eel-mime-application-chooser.c:479
msgid "No applications selected"
msgstr "لم يتم انتقاء أيّة تطبيقات"

#: ../eel/eel-mime-application-chooser.c:528 ../eel/eel-open-with-dialog.c:984
#, c-format
msgid "%s document"
msgstr "مستند %s"

#: ../eel/eel-mime-application-chooser.c:538 ../eel/eel-open-with-dialog.c:997
msgid "Unknown"
msgstr "مجهول"

#: ../eel/eel-mime-application-chooser.c:546
#, c-format
msgid "Select an application to open %s and others of type \"%s\""
msgstr "اختر تطبيقاً لفتح <i>%s</i> و غيره من الفئة \"%s\""

#: ../eel/eel-open-with-dialog.c:175 ../eel/eel-open-with-dialog.c:220
msgid "Could not run application"
msgstr "تعذّر تشغيل التّطبيق"

#: ../eel/eel-open-with-dialog.c:187
#, c-format
msgid "Could not find '%s'"
msgstr "تعذّر العثور على '%s'"

#: ../eel/eel-open-with-dialog.c:190
msgid "Could not find application"
msgstr "تعذّر العثور على التّطبيق"

#: ../eel/eel-open-with-dialog.c:287
msgid "Could not add application"
msgstr "تعذّر إضافة التّطبيق"

#: ../eel/eel-open-with-dialog.c:288
msgid "Could not add application to the application database"
msgstr "تعذّر إضافة التّطبيق لقاعدة بيانات التّطبيقات"

#: ../eel/eel-open-with-dialog.c:401
msgid "Select an Application"
msgstr "اختر تطبيقاً"

#: ../eel/eel-open-with-dialog.c:809
msgid "Open With"
msgstr "افتح بـاستخدام"

#: ../eel/eel-open-with-dialog.c:846
msgid "Select an application to view its description."
msgstr "اختر تطبيقاً لعرض وصفه."

#: ../eel/eel-open-with-dialog.c:871
msgid "_Use a custom command"
msgstr "ا_ستعمل امرا مخصصا"

#: ../eel/eel-open-with-dialog.c:888
msgid "_Browse..."
msgstr "_تصفّح..."

#: ../eel/eel-open-with-dialog.c:910
msgid "_Open"
msgstr "ا_فتح"

#: ../eel/eel-open-with-dialog.c:1004
#, c-format
msgid "Open %s and other files of type \"%s\" with:"
msgstr "افتح <i>%s</i> و غيره من الملفّات من فئة \"%s\" باستخدام:"

#: ../eel/eel-open-with-dialog.c:1036
msgid "_Add"
msgstr "أ_ضف"

#: ../eel/eel-open-with-dialog.c:1037
msgid "Add Application"
msgstr "أضِف تطبيق"

#: ../eel/eel-stock-dialogs.c:214
msgid "You can stop this operation by clicking cancel."
msgstr "يمكنك ايقاف هذه العمليّة بنقر إلغاء."

#: ../eel/eel-vfs-extensions.c:650
msgid " (invalid Unicode)"
msgstr " (يونيكود غير صحيح)"

