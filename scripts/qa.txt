# commented lines like this will be ignored
# english, correct translation, wrong translation
password, كلمة السر, كلمة المرور
password, كلمة سر, كلمة مرور
password, كلمة سر, كلمة عبور
password, كلمة السر, كلمة العبور
username, اسم المستخدم :: اسم مستخدم
usernames, اسماء المستخدمين :: اسماء مستخدمين
Error, خطأ, خطاء
error, خطأ, خطاء
schema, مخطط, سيمة
Schema, مخطط, سيمة
, اسم, إسم
, احذف, إحذف
, الغِ, الغي
, الغِ, إلغي
, الغِ, إلغ
, سمة, تيمة
choose, انتقِ, انتقي
Choose, انتقِ, انتقي
choose, انتقِ, اختر
Choose, انتقِ, اختر
choose, انتقاء, اختيار
Choose, انتقاء, اختيار
, لا, ﻻ
, لأ, ﻷ
, لإ, ﻹ
