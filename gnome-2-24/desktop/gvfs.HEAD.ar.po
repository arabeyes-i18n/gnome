# translation of gvfs.HEAD.po to Arabic
# Copyright (C) 2008 THE gnome'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome package.
#
#
# Djihed Afifi <djihed@gmail.com> 2008.
# Khaled Hosny <khaledhosny@eglug.org>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: gvfs.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-06-11 22:30+0100\n"
"PO-Revision-Date: 2008-05-22 19:21+0300\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Arabic\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: ../client/gdaemonfile.c:482 ../client/gdaemonfile.c:1977
msgid "Operation not supported, files on different mounts"
msgstr "العملية غير مدعومة، ملفات على نقاط وصْل مختلفة"

#: ../client/gdaemonfile.c:787
msgid "Invalid return value from get_info"
msgstr "قيمة مرتجعة غير صحيحة من get_info"

#: ../client/gdaemonfile.c:816 ../client/gdaemonfile.c:1563
msgid "Invalid return value from query_info"
msgstr "قيمة مرتجعة غير صحيحة من query_info"

#: ../client/gdaemonfile.c:894
msgid "Couldn't get stream file descriptor"
msgstr "تعذر جلب واصِف ملف الدفْق"

#: ../client/gdaemonfile.c:926 ../client/gdaemonfile.c:996
#: ../client/gdaemonfile.c:1055 ../client/gdaemonfile.c:1114
#: ../client/gdaemonfile.c:1176 ../client/gdaemonfile.c:2242
#: ../client/gdaemonfile.c:2330 ../client/gdaemonfile.c:2599
msgid "Invalid return value from open"
msgstr "قيمة غير صحيحة للدفْق من \"افتح\""

#: ../client/gdaemonfile.c:1006 ../client/gdaemonfile.c:1065
#: ../client/gdaemonfile.c:1124 ../client/gdaemonfile.c:1186
#: ../client/gdaemonfile.c:2200
msgid "Didn't get stream file descriptor"
msgstr "لم أحصل على واصِف ملف الدفْق"

#: ../client/gdaemonfile.c:1238 ../client/gdaemonfile.c:1255
msgid "Invalid return value from call"
msgstr "قيمة مُرتجعة غير صحيحة من النداء"

#: ../client/gdaemonfile.c:1534
msgid "Invalid return value from get_filesystem_info"
msgstr "قيمة مُرتجعة غير صحيحة من get_filesystem_info"

#. translators: this is an error message when there is no user visible "mount" object
#. corresponding to a particular path/uri
#: ../client/gdaemonfile.c:1647
msgid "Could not find enclosing mount"
msgstr "تعذّر العثور على نقطة الضم الحاوية"

#: ../client/gdaemonfile.c:1677
#, c-format
msgid "Invalid filename %s"
msgstr "اسم ملف غير صالح %s"

#: ../client/gdaemonfile.c:1719
msgid "Invalid return value from query_filesystem_info"
msgstr "قيمة مُرتجعة غير صحيحة من query_filesystem_info"

#: ../client/gdaemonfile.c:2122
msgid "Invalid return value from monitor_dir"
msgstr "قيمة مُرتجعة غير صحيحة من monitor_dir"

#: ../client/gdaemonfile.c:2171
msgid "Invalid return value from monitor_file"
msgstr "قيمة مُرتجعة غير صحيحة من monitor_file"

#: ../client/gdaemonfileinputstream.c:451
#: ../client/gdaemonfileinputstream.c:459
#: ../client/gdaemonfileinputstream.c:1304
#: ../client/gdaemonfileinputstream.c:1314
#: ../client/gdaemonfileoutputstream.c:403
#: ../client/gdaemonfileoutputstream.c:411
#: ../client/gdaemonfileoutputstream.c:1052
#: ../client/gdaemonfileoutputstream.c:1062
#, c-format
msgid "Error in stream protocol: %s"
msgstr "خطأ في برتوكول الدفْق: %s"

#: ../client/gdaemonfileinputstream.c:459
#: ../client/gdaemonfileinputstream.c:1314
#: ../client/gdaemonfileoutputstream.c:411
#: ../client/gdaemonfileoutputstream.c:1062
msgid "End of stream"
msgstr "نهاية الدفْق"

#: ../client/gdaemonfileinputstream.c:519
#: ../client/gdaemonfileinputstream.c:676
#: ../client/gdaemonfileinputstream.c:785
#: ../client/gdaemonfileinputstream.c:1036
#: ../client/gdaemonfileoutputstream.c:460
#: ../client/gdaemonfileoutputstream.c:639
#: ../client/gdaemonfileoutputstream.c:835 ../daemon/gvfsbackendobexftp.c:774
#: ../daemon/gvfsbackendobexftp.c:795 ../daemon/gvfsbackendobexftp.c:914
#: ../daemon/gvfsbackendobexftp.c:1051 ../daemon/gvfsbackendobexftp.c:1115
#: ../daemon/gvfsbackendobexftp.c:1252 ../daemon/gvfsbackendobexftp.c:1279
#: ../daemon/gvfsbackendobexftp.c:1338 ../daemon/gvfsbackendobexftp.c:1360
#: ../daemon/gvfsbackendobexftp.c:1420 ../daemon/gvfsbackendobexftp.c:1439
#: ../daemon/gvfsbackendsmb.c:1071 ../daemon/gvfsbackendtest.c:87
#: ../daemon/gvfsbackendtest.c:120 ../daemon/gvfsbackendtest.c:183
#: ../daemon/gvfschannel.c:298
msgid "Operation was cancelled"
msgstr "أُلغيت العملية"

#: ../client/gdaemonfileinputstream.c:1211
#: ../client/gdaemonfileoutputstream.c:946
msgid "Seek not supported on stream"
msgstr "لا يُدعم الالتماس على الدفْق"

#: ../client/gdaemonfileinputstream.c:1241
msgid "The query info operation is not supported"
msgstr "لا تُدعم عملية استعلام المعلومات"

#: ../client/gdaemonvfs.c:733
#, c-format
msgid "Error while getting mount info: %s"
msgstr "خطأ أثناء جلب معلومات الوصْل: %s"

#: ../client/gvfsdaemondbus.c:568 ../client/gvfsdaemondbus.c:999
#, c-format
msgid "Error connecting to daemon: %s"
msgstr "خطأ أثناء الاتصال بالخادوم: %s"

#: ../common/gsysutils.c:136
#, c-format
msgid "Error creating socket: %s"
msgstr "خطأ أثناء إنشاء المقبس: %s"

#: ../common/gsysutils.c:174
#, c-format
msgid "Error connecting to socket: %s"
msgstr "خطأ أثناء الاتصال بالمقبس: %s"

#: ../common/gvfsdaemonprotocol.c:515
msgid "Invalid file info format"
msgstr "تنسيق ملف معلومات غير صحيح."

#: ../common/gvfsdaemonprotocol.c:532
msgid "Invalid attribute info list content"
msgstr "محتوى قائمة خواص غير صحيح"

#: ../daemon/daemon-main.c:63 ../daemon/daemon-main.c:206
#, c-format
msgid "Error connecting to D-Bus: %s"
msgstr "خطأ أثناء الاتصال بـ D-Bus: %s"

#. translators: This is the default daemon's application name,
#. * the %s is the type of the backend, like "ftp"
#: ../daemon/daemon-main.c:78
#, c-format
msgid "%s Filesystem Service"
msgstr "خدمة نظام ملفات %s"

#: ../daemon/daemon-main.c:97
#, c-format
msgid "Error: %s"
msgstr "خطأ: %s"

#: ../daemon/daemon-main.c:131
#, c-format
msgid "Usage: %s --spawner dbus-id object_path"
msgstr "الاستخدام: ‪%s --spawner dbus-id object_path‬"

#: ../daemon/daemon-main.c:155 ../daemon/daemon-main.c:173
#, c-format
msgid "Usage: %s key=value key=value ..."
msgstr "الاستخدام: ‪%s key=value key=value ...‬"

#: ../daemon/daemon-main.c:171
#, c-format
msgid "No mount type specified"
msgstr "لم يُحدّد نوع الوصْل"

#: ../daemon/daemon-main.c:241
#, c-format
msgid "mountpoint for %s already running"
msgstr "نقطة الوصل %s تعمل مسبقا"

#: ../daemon/daemon-main.c:252
msgid "error starting mount daemon"
msgstr "خطأ أثناء بدأ جني الوصْل"

#. FIXME: this should really be "/ in %s", but can't change
#. due to string freeze.
#. Translators: This is the name of the root of an sftp share, like "/ on <hostname>"
#: ../daemon/gvfsbackendarchive.c:315 ../daemon/gvfsbackendftp.c:963
#: ../daemon/gvfsbackendsftp.c:1893
#, c-format
msgid "/ on %s"
msgstr "/ على %s"

#: ../daemon/gvfsbackendarchive.c:515 ../daemon/gvfsbackendftp.c:1479
#: ../daemon/gvfsbackendsftp.c:1552
msgid "No hostname specified"
msgstr "لم يحدّد اسم مستضيف"

#: ../daemon/gvfsbackendarchive.c:526 ../daemon/gvfsbackendarchive.c:555
#: ../daemon/gvfsbackenddav.c:1446 ../daemon/gvfsbackendhttp.c:299
#: ../daemon/gvfsbackendobexftp.c:605 ../daemon/gvfsbackendobexftp.c:616
#: ../daemon/gvfsbackendsmb.c:605
msgid "Invalid mount spec"
msgstr "وصف وصْل خطأ"

#: ../daemon/gvfsbackendarchive.c:636 ../daemon/gvfsbackendarchive.c:686
#: ../daemon/gvfsbackendarchive.c:715 ../daemon/gvfsbackendcdda.c:823
#: ../daemon/gvfsbackendcomputer.c:572 ../daemon/gvfsbackenddnssd.c:345
#: ../daemon/gvfsbackendftp.c:2101 ../daemon/gvfsbackendnetwork.c:590
#: ../daemon/gvfsbackendsmbbrowse.c:804 ../daemon/gvfsbackendsmbbrowse.c:871
#: ../daemon/gvfsbackendsmbbrowse.c:1042 ../daemon/gvfsbackendsmbbrowse.c:1116
#, c-format
msgid "File doesn't exist"
msgstr "الملف غير موجود"

#: ../daemon/gvfsbackendarchive.c:724 ../daemon/gvfsbackendburn.c:680
#: ../daemon/gvfsbackendcomputer.c:651 ../daemon/gvfsbackenddnssd.c:393
#: ../daemon/gvfsbackendftp.c:912 ../daemon/gvfsbackendnetwork.c:637
#, c-format
msgid "The file is not a directory"
msgstr "الملف ليس مُجلّدا"

#. Translators: This is the name of the backend
#: ../daemon/gvfsbackendburn.c:346
msgid "Burn"
msgstr "اكتب"

#: ../daemon/gvfsbackendburn.c:376
msgid "Unable to create temporary directory"
msgstr "تعذّر إنشاء المجلد المؤقت"

#: ../daemon/gvfsbackendburn.c:405 ../daemon/gvfsbackendburn.c:416
#: ../daemon/gvfsbackendburn.c:451 ../daemon/gvfsbackendburn.c:672
#: ../daemon/gvfsbackendburn.c:719 ../daemon/gvfsbackendburn.c:745
#: ../daemon/gvfsbackendburn.c:783
msgid "No such file or directory"
msgstr "ليس ثمّة ملف أو مجلد"

#: ../daemon/gvfsbackendburn.c:425 ../daemon/gvfsbackenddav.c:2033
msgid "Directory not empty"
msgstr "الدليل غير فارغ"

#: ../daemon/gvfsbackendburn.c:459 ../daemon/gvfsbackendburn.c:897
msgid "Can't copy file over directory"
msgstr "لا يمكن نسخ ملف على مجلد"

#. Translators: this is the display name of the backend
#: ../daemon/gvfsbackendburn.c:651
msgid "CD/DVD Creator"
msgstr "منشئ الاسطوانات/ديڤيدي"

#: ../daemon/gvfsbackendburn.c:755 ../daemon/gvfsbackendburn.c:791
#: ../daemon/gvfsbackendburn.c:917
msgid "File exists"
msgstr "الملف موجود"

#: ../daemon/gvfsbackendburn.c:845
msgid "No such file or directory in target path"
msgstr "ليس ثمّة ملف أو مجلد في المسار المقصود"

#: ../daemon/gvfsbackendburn.c:868
msgid "Can't copy directory over directory"
msgstr "لا يمكن نسخ دليل على دليل"

#: ../daemon/gvfsbackendburn.c:877
msgid "Target file exists"
msgstr "الملف الهدف موجود مسبّقا"

#: ../daemon/gvfsbackendburn.c:884
msgid "Can't recursively copy directory"
msgstr "تعذّر النسخ التتابعي للدليل "

#: ../daemon/gvfsbackendburn.c:943
msgid "Not supported"
msgstr "ليس مدعومًا"

#: ../daemon/gvfsbackendcdda.c:262 ../daemon/gvfsbackendcdda.c:333
msgid "No drive specified"
msgstr "لم يحدّد أي مشغّل"

#: ../daemon/gvfsbackendcdda.c:277
#, c-format
msgid "Cannot find drive %s"
msgstr "تعذّر إيجاد المُشغّل %s"

#: ../daemon/gvfsbackendcdda.c:287
#, c-format
msgid "Drive %s does not contain audio files"
msgstr "لا يحتوي المشغل %s على ملفات صوت"

#. Translator: %s is the device the disc is inserted into
#: ../daemon/gvfsbackendcdda.c:295
#, c-format
msgid "cdda mount on %s"
msgstr "نقطة تحميل cdda2 على %s"

#: ../daemon/gvfsbackendcdda.c:296 ../daemon/gvfsbackendcdda.c:800
#: ../hal/ghalmount.c:445 ../hal/ghalvolume.c:256 ../hal/ghalvolume.c:277
#, c-format
msgid "Audio Disc"
msgstr "قرص صوتي"

#: ../daemon/gvfsbackendcdda.c:357
#, c-format
msgid "File system is busy: %d open file"
msgid_plural "File system is busy: %d open files"
msgstr[0] "نظام الملفات مشغول: لا ملفات مفتوحة"
msgstr[1] "نظام الملفات مشغول: ملف واحد مفتوح"
msgstr[2] "نظام الملفات مشغول: ملفين مفتوحين"
msgstr[3] "نظام الملفات مشغول: %Id ملفات مفتوحة"
msgstr[4] "نظام الملفات مشغول: %Id ملفا مفتوحا"
msgstr[5] "نظام الملفات مشغول: %Id ملف مفتوح"

#: ../daemon/gvfsbackendcdda.c:547
#, c-format
msgid "No such file %s on drive %s"
msgstr "ليس ثمّة ملف %s على المشغّل %s"

#. Translators: paranoia is the name of the cd audio reading library
#: ../daemon/gvfsbackendcdda.c:656
#, c-format
msgid "Error from 'paranoia' on drive %s"
msgstr "خطأ من 'paranoia' على المشغّل %s"

#: ../daemon/gvfsbackendcdda.c:719
#, c-format
msgid "Error seeking in stream on drive %s"
msgstr "خطأ أثناء الالتماس في الدفق على المشغّل %s"

#: ../daemon/gvfsbackendcdda.c:816 ../daemon/gvfsbackendgphoto2.c:1696
#, c-format
msgid "No such file"
msgstr "ليس ثمّة ملف"

#: ../daemon/gvfsbackendcdda.c:830
#, c-format
msgid "The file does not exist or isn't an audio track"
msgstr "الملف غير موجود أو ليس مقطعا صوتيا"

#: ../daemon/gvfsbackendcdda.c:936
msgid "Audio CD Filesystem Service"
msgstr "خدمة نظام ملفات أقراص الصوت"

#: ../daemon/gvfsbackendcomputer.c:179 ../daemon/gvfsbackendcomputer.c:693
msgid "Computer"
msgstr "الحاسوب"

#: ../daemon/gvfsbackendcomputer.c:466
msgid "Filesystem"
msgstr "نظام الملفات"

#: ../daemon/gvfsbackendcomputer.c:590 ../daemon/gvfsbackendgphoto2.c:1688
#: ../daemon/gvfsbackendobexftp.c:760 ../daemon/gvfsbackendsmb.c:648
#: ../daemon/gvfsbackendtrash.c:655
msgid "Can't open directory"
msgstr "تعذّر فتح الدّليل"

#: ../daemon/gvfsbackendcomputer.c:594 ../daemon/gvfsbackendcomputer.c:732
msgid "Can't open mountable file"
msgstr "تعذّر فتح الملف القابل للوصْل"

#: ../daemon/gvfsbackendcomputer.c:780 ../daemon/gvfsbackendsftp.c:1036
#, c-format
msgid "Internal error: %s"
msgstr "خطأ داخلي: %s"

#: ../daemon/gvfsbackendcomputer.c:812 ../daemon/gvfsbackendcomputer.c:929
msgid "Can't mount file"
msgstr "تعذّر وصْل الملف"

#: ../daemon/gvfsbackendcomputer.c:824
msgid "No media in the drive"
msgstr "لا وسائط في القرص"

#: ../daemon/gvfsbackendcomputer.c:881 ../daemon/gvfsbackendcomputer.c:972
#: ../daemon/gvfsbackendcomputer.c:1072
msgid "Not a mountable file"
msgstr "لا ملف قابل للوصْل"

#: ../daemon/gvfsbackendcomputer.c:987
msgid "Can't unmount file"
msgstr "تعذّر فصْل الملف"

#: ../daemon/gvfsbackendcomputer.c:1103
msgid "Can't eject file"
msgstr "تعذّر إخراج الملف"

#: ../daemon/gvfsbackenddav.c:504 ../daemon/gvfsbackenddav.c:1513
#: ../daemon/gvfsbackendhttp.c:245
#, c-format
msgid "HTTP Error: %s"
msgstr "خطأ HTTP‬: ‫%s‪"

#: ../daemon/gvfsbackenddav.c:520
msgid "Could not parse response"
msgstr "تعذّر تحليل الاستجابة"

#: ../daemon/gvfsbackenddav.c:529
msgid "Empty response"
msgstr "استجابة خالية"

#: ../daemon/gvfsbackenddav.c:536
msgid "Unexpected reply from server"
msgstr "ردّ غير متوقّع من الخادوم"

#: ../daemon/gvfsbackenddav.c:1146 ../daemon/gvfsbackenddav.c:1648
#, c-format
msgid "Response invalid"
msgstr "رد غير سليم"

#: ../daemon/gvfsbackenddav.c:1289
msgid "WebDAV share"
msgstr "مشاركة WebDAV"

#: ../daemon/gvfsbackenddav.c:1291
#, c-format
msgid "Enter password for %s"
msgstr "أدخل كلمة سر %s"

#: ../daemon/gvfsbackenddav.c:1294
msgid "Please enter proxy password"
msgstr "رجاءً أدخل كلمة سر الوسيط"

#: ../daemon/gvfsbackenddav.c:1517 ../daemon/gvfsbackenddav.c:1521
msgid "Not a WebDAV enabled share"
msgstr "ليس مشاركة WebDAV مفعّلة"

#: ../daemon/gvfsbackenddav.c:1544
#, c-format
msgid "WebDAV on %s"
msgstr "WebDAV على %s"

#: ../daemon/gvfsbackenddav.c:1604 ../daemon/gvfsbackenddav.c:1677
msgid "Could not create request"
msgstr "تعذّر إنشاء الطلب"

#: ../daemon/gvfsbackenddav.c:1740 ../daemon/gvfsbackenddav.c:1993
#: ../daemon/gvfsbackenddav.c:2104 ../daemon/gvfsbackendftp.c:1699
#: ../daemon/gvfsbackendftp.c:2365 ../daemon/gvfsbackendsftp.c:3535
#: ../daemon/gvfsbackendsmb.c:1807
#, c-format
msgid "Target file already exists"
msgstr "الملف الهدف موجود مسبقًا"

#: ../daemon/gvfsbackenddav.c:1813 ../daemon/gvfsbackendsftp.c:2783
#: ../daemon/gvfsbackendsmb.c:1039
msgid "The file was externally modified"
msgstr "عُدّل الملف خارجيّا"

#: ../daemon/gvfsbackenddav.c:1844 ../daemon/gvfsbackendsmb.c:1077
#: ../daemon/gvfsbackendsmb.c:1824
msgid "Backup file creation failed"
msgstr "فشل إنشاء الملف الاحتياطي"

#. TODO: Name
#: ../daemon/gvfsbackenddnssd.c:433
msgid "dns-sd"
msgstr "dns-sd"

#: ../daemon/gvfsbackenddnssd.c:669 ../daemon/gvfsbackendnetwork.c:731
msgid "Can't monitor file or directory."
msgstr "لا يمكن مراقبة الملف أو الدليل"

#. TODO: Names, etc
#: ../daemon/gvfsbackenddnssd.c:687
msgid "Dns-SD"
msgstr "Dns-SD"

#: ../daemon/gvfsbackenddnssd.c:688 ../daemon/gvfsbackendnetwork.c:676
#: ../daemon/gvfsbackendnetwork.c:814 ../daemon/gvfsbackendnetwork.c:815
msgid "Network"
msgstr "الشبكة"

#: ../daemon/gvfsbackendftp.c:236
msgid "Accounts are unsupported"
msgstr "الحسابات غير مدعومة"

#: ../daemon/gvfsbackendftp.c:240
msgid "Host closed connection"
msgstr "قطع الخادوم الاتصال"

#: ../daemon/gvfsbackendftp.c:244
msgid "Cannot open data connection. Maybe your firewall prevents this?"
msgstr "لا يمكن فتح اتصال. هل هناك حائط ناري؟"

#: ../daemon/gvfsbackendftp.c:248
msgid "Data connection closed"
msgstr "أُغلق اتصال البيانات"

#: ../daemon/gvfsbackendftp.c:255 ../daemon/gvfsbackendftp.c:259
msgid "Operation failed"
msgstr "فشلت العمليّة"

#: ../daemon/gvfsbackendftp.c:264
msgid "No space left on server"
msgstr "لا توجد مساحة كافية على الخادوم"

#: ../daemon/gvfsbackendftp.c:272 ../daemon/gvfsbackendsftp.c:3844
msgid "Operation unsupported"
msgstr "عمليّة غير مدعومة"

#: ../daemon/gvfsbackendftp.c:276 ../daemon/gvfsbackendsftp.c:288
msgid "Permission denied"
msgstr "رُفض التّصريح"

#: ../daemon/gvfsbackendftp.c:280
msgid "Page type unknown"
msgstr "نوع الصّفحة غير معروف"

#: ../daemon/gvfsbackendftp.c:284 ../daemon/gvfsbackendftp.c:2211
#, c-format
msgid "Invalid filename"
msgstr "اسم ملف غير صالح"

#: ../daemon/gvfsbackendftp.c:288 ../daemon/gvfsbackendftp.c:364
#: ../daemon/gvfsbackendftp.c:393 ../daemon/gvfsbackendftp.c:412
#: ../daemon/gvfsbackendftp.c:425 ../daemon/gvfsbackendftp.c:817
#, c-format
msgid "Invalid reply"
msgstr "رد غير صالح"

#: ../daemon/gvfsbackendftp.c:532
#, c-format
msgid "broken transmission"
msgstr "إرسال معطّل"

#: ../daemon/gvfsbackendftp.c:677 ../daemon/gvfsbackendftp.c:837
#, c-format
msgid "Could not connect to host"
msgstr "تعذّر الاتصال بالمستضيف"

#. translators: %s here is the hostname
#: ../daemon/gvfsbackendftp.c:1337
#, c-format
msgid "Enter password for ftp on %s"
msgstr "رجاء أدخل كلمة سر آف تي بي (ftp) على %s"

#: ../daemon/gvfsbackendftp.c:1362 ../daemon/gvfsbackendsftp.c:873
msgid "Password dialog cancelled"
msgstr "أُلغِي حوار كلمة السر"

#: ../daemon/gvfsbackendftp.c:1441
#, c-format
msgid "ftp on %s"
msgstr "آف تي بي (ftp) على %s"

#. Translators: the first %s is the username, the second the host name
#: ../daemon/gvfsbackendftp.c:1445
#, c-format
msgid "ftp as %s on %s"
msgstr "آف تي بي (ftp) باسم %s على %s"

#: ../daemon/gvfsbackendftp.c:1529 ../daemon/gvfsbackendsftp.c:1957
#: ../daemon/gvfsbackendsftp.c:3526
#, c-format
msgid "File is directory"
msgstr "الملف دليل"

#: ../daemon/gvfsbackendftp.c:1751 ../daemon/gvfsbackendftp.c:2326
msgid "backups not supported yet"
msgstr "النسخ الاحتياطية غير مدعومة"

#: ../daemon/gvfsbackendftp.c:1828
#, c-format
msgid "filename too long"
msgstr "اسم الملف طويل جدًا"

#: ../daemon/gvfsbackendftp.c:2345
#, c-format
msgid "Invalid destination filename"
msgstr "اسم الملف الهدف غير سليم"

#. Translator: %s represents the device, e.g. usb:001,042
#: ../daemon/gvfsbackendgphoto2.c:696
#, c-format
msgid "Digital Camera (%s)"
msgstr "آلة تصوير رقميّة (%s)"

#. Translator: %s is the vendor name, e.g. Panasonic
#. Translators: %s is the device vendor
#: ../daemon/gvfsbackendgphoto2.c:842 ../hal/ghalvolume.c:358
#, c-format
msgid "%s Camera"
msgstr "آلة تصوير %s"

#. Translator: %s is the vendor name, e.g. Panasonic
#. Translators: %s is the device vendor
#: ../daemon/gvfsbackendgphoto2.c:845 ../hal/ghalvolume.c:353
#, c-format
msgid "%s Audio Player"
msgstr "مشغل صوت %s"

#: ../daemon/gvfsbackendgphoto2.c:854 ../hal/ghalvolume.c:367
msgid "Camera"
msgstr "آلة تصوير"

#: ../daemon/gvfsbackendgphoto2.c:856 ../hal/ghalvolume.c:365
msgid "Audio Player"
msgstr "مشغل الصوت"

#. Translator: %s represents the device, e.g. usb:001,042
#: ../daemon/gvfsbackendgphoto2.c:1540
#, c-format
msgid "gphoto2 mount on %s"
msgstr "نقطة وصْل gphoto2 على %s"

#: ../daemon/gvfsbackendhttp.c:241
#, c-format
msgid "HTTP Client Error: %s"
msgstr "خطأ عميل ‪HTTP‬: ‫%s"

#: ../daemon/gvfsbackendhttp.c:560 ../daemon/gvfsbackendsftp.c:1881
#: ../daemon/gvfsbackendsmb.c:1304 ../daemon/gvfsbackendtrash.c:983
#: ../daemon/gvfsdaemonutils.c:211
msgid " (invalid encoding)"
msgstr "  (ترميز غير سليم)"

#: ../daemon/gvfsbackendlocaltest.c:853
msgid "Directory notification not supported"
msgstr "تنبيه المجلد غير مدعوم"

#. smb:/// root link
#: ../daemon/gvfsbackendnetwork.c:268 ../daemon/gvfsbackendsmbbrowse.c:699
msgid "Windows Network"
msgstr "شبكة ويندوز"

#. "separate": a link to dns-sd://local/
#: ../daemon/gvfsbackendnetwork.c:392
msgid "Local Network"
msgstr "شبكة محليّة"

#. Translators: this is the friendly name of the 'network://' backend that
#. * shows computers in your local network.
#: ../daemon/gvfsbackendnetwork.c:862
msgid "Network Location Monitor"
msgstr "مراقب أمكنة الشبكة"

#. Mount was successful
#. Translators: This is "<sharename> on <servername>" and is used as name for an SMB share
#: ../daemon/gvfsbackendobexftp.c:444 ../daemon/gvfsbackendsmb.c:557
#: ../daemon/gvfsbackendsmb.c:1297
#, c-format
msgid "%s on %s"
msgstr "‏%s على %s"

#: ../daemon/gvfsbackendsftp.c:280
msgid "ssh program unexpectedly exited"
msgstr "خرج برنامج ssh بصورة غير متوقّعة"

#: ../daemon/gvfsbackendsftp.c:295
msgid "Hostname not known"
msgstr "اسم المستضيف مجهول"

#: ../daemon/gvfsbackendsftp.c:302
msgid "No route to host"
msgstr "لا توجيه للمستضيف"

#: ../daemon/gvfsbackendsftp.c:309
msgid "Connection refused by server"
msgstr "رفض الخادوم الاتصال"

#: ../daemon/gvfsbackendsftp.c:316
msgid "Host key verification failed"
msgstr "فشل التحقق من مفتاح المستضيف"

#: ../daemon/gvfsbackendsftp.c:399
msgid "Unable to spawn ssh program"
msgstr "فشل تشعيب برنامج ssh"

#: ../daemon/gvfsbackendsftp.c:415
#, c-format
msgid "Unable to spawn ssh program: %s"
msgstr "فشل تشعيب برنامج ‪ssh‬: ‫%s"

#: ../daemon/gvfsbackendsftp.c:527 ../daemon/gvfsbackendsftp.c:779
msgid "Timed out when logging in"
msgstr "انتهت المهلة أثناء الولوج"

#: ../daemon/gvfsbackendsftp.c:857
msgid "Enter passphrase for key"
msgstr "أدخل عبارة سر المفتاح"

#: ../daemon/gvfsbackendsftp.c:859
msgid "Enter password"
msgstr "أدخل كلمة السر"

#: ../daemon/gvfsbackendsftp.c:920
msgid "Can't send password"
msgstr "تعذّر إرسال كلمة السر"

#: ../daemon/gvfsbackendsftp.c:928
msgid "Log In Anyway"
msgstr "لُج على أي حال"

#: ../daemon/gvfsbackendsftp.c:928
msgid "Cancel Login"
msgstr "ألغِ الولوج"

#: ../daemon/gvfsbackendsftp.c:938
#, c-format
msgid ""
"The identity of the remote computer (%s) is unknown.\n"
"This happens when you log in to a computer the first time.\n"
"\n"
"The identity sent by the remote computer is %s. If you want to be absolutely "
"sure it is safe to continue, contact the system administrator."
msgstr ""
"هوية الحاسوب البعيد (%s) غير معروفة.\n"
"يحدث هذا عند الولوج إلى حاسوب للمرّة الأولى.\n"
"\n"
"الهوية المرسلة من الحاسوب البعيد هي %s. إذا أردت التأكد من سلامة الاستمرار، "
"اتصل بمدير النظام."

#: ../daemon/gvfsbackendsftp.c:958
msgid "Login dialog cancelled"
msgstr "أُلغِي حوار الولوج"

#: ../daemon/gvfsbackendsftp.c:978
msgid "Can't send host identity confirmation"
msgstr "لا يمكن إرسال تأكيد هوية الخادوم"

#: ../daemon/gvfsbackendsftp.c:1471 ../daemon/gvfsbackendsftp.c:1494
msgid "Protocol error"
msgstr "خطأ بروتوكول"

#. Translators: This is the name of an sftp share, like "sftp on <hostname>"
#: ../daemon/gvfsbackendsftp.c:1518
#, c-format
msgid "sftp on %s"
msgstr "‏sftp على %s"

#: ../daemon/gvfsbackendsftp.c:1542
msgid "Unable to find supported ssh command"
msgstr "تعذّر إيجاد أمر ssh مدعوم"

#: ../daemon/gvfsbackendsftp.c:1966
msgid "Failure"
msgstr "فشل"

#: ../daemon/gvfsbackendsftp.c:2022 ../daemon/gvfsbackendsftp.c:2081
#: ../daemon/gvfsbackendsftp.c:2092 ../daemon/gvfsbackendsftp.c:2148
#: ../daemon/gvfsbackendsftp.c:2234 ../daemon/gvfsbackendsftp.c:2262
#: ../daemon/gvfsbackendsftp.c:2308 ../daemon/gvfsbackendsftp.c:2383
#: ../daemon/gvfsbackendsftp.c:2490 ../daemon/gvfsbackendsftp.c:2530
#: ../daemon/gvfsbackendsftp.c:2580 ../daemon/gvfsbackendsftp.c:2649
#: ../daemon/gvfsbackendsftp.c:2669 ../daemon/gvfsbackendsftp.c:2820
#: ../daemon/gvfsbackendsftp.c:2845 ../daemon/gvfsbackendsftp.c:2900
#: ../daemon/gvfsbackendsftp.c:2957 ../daemon/gvfsbackendsftp.c:3228
#: ../daemon/gvfsbackendsftp.c:3295 ../daemon/gvfsbackendsftp.c:3424
#: ../daemon/gvfsbackendsftp.c:3459 ../daemon/gvfsbackendsftp.c:3487
#: ../daemon/gvfsbackendsftp.c:3595 ../daemon/gvfsbackendsftp.c:3649
#: ../daemon/gvfsbackendsftp.c:3683 ../daemon/gvfsbackendsftp.c:3717
#: ../daemon/gvfsbackendsftp.c:3732 ../daemon/gvfsbackendsftp.c:3747
#: ../daemon/gvfsbackendsftp.c:3825
msgid "Invalid reply received"
msgstr "تم استقبال رد غير صحيح"

#: ../daemon/gvfsbackendsftp.c:2328
#, c-format
msgid "Error creating backup file: %s"
msgstr "خطأ في إنشاء الملف الاحتياطي: %s"

#: ../daemon/gvfsbackendsftp.c:2730
msgid "Unable to create temporary file"
msgstr "تعذّر إنشاء الملف المؤقت"

#: ../daemon/gvfsbackendsftp.c:3521 ../daemon/gvfsbackendsmb.c:1796
msgid "Can't move directory over directory"
msgstr "لا يمكن نقل دليل على دليل"

#. translators: First %s is a share name, second is a server name
#: ../daemon/gvfsbackendsmb.c:216
#, c-format
msgid "Password required for share %s on %s"
msgstr "كلمة السر مطلوبة لمشاركة %s على %s"

#: ../daemon/gvfsbackendsmb.c:471 ../daemon/gvfsbackendsmb.c:511
#: ../daemon/gvfsdaemonutils.c:92
#, c-format
msgid "Internal Error (%s)"
msgstr "خطأ داخلي (%s)"

#. translators: We tried to mount a windows (samba) share, but failed
#: ../daemon/gvfsbackendsmb.c:550
msgid "Failed to mount Windows share"
msgstr "فشل وصْل مشاركة ويندوز"

#: ../daemon/gvfsbackendsmb.c:717 ../daemon/gvfsbackendsmb.c:1176
msgid "Unsupported seek type"
msgstr "نوع التماس غير مدعوم"

#: ../daemon/gvfsbackendsmb.c:1240
#, c-format
msgid "Backup file creation failed: %s"
msgstr "فشل إنشاء ملف تخزين: %s"

#: ../daemon/gvfsbackendsmb.c:1698
#, c-format
msgid "Error deleting file: %s"
msgstr "خطأ أثناء حذف الملف: %s"

#: ../daemon/gvfsbackendsmb.c:1772
#, c-format
msgid "Error moving file: %s"
msgstr "خطأ أثناء نقل الملف: %s"

#: ../daemon/gvfsbackendsmb.c:1844
#, c-format
msgid "Error removing target file: %s"
msgstr "خطأ في إزالة الملف الهدف: %s"

#: ../daemon/gvfsbackendsmb.c:1868
msgid "Can't recursively move directory"
msgstr "تعذّر نقل المجلد تتابعيا"

#: ../daemon/gvfsbackendsmb.c:1931
msgid "Windows Shares Filesystem Service"
msgstr "خادوم نظام ملفات مشاركات ويندوز"

#. translators: Name for the location that lists the smb shares
#. availible on a server (%s is the name of the server)
#: ../daemon/gvfsbackendsmbbrowse.c:707
#, c-format
msgid "Windows shares on %s"
msgstr "مشاركات ويندوز على %s"

#: ../daemon/gvfsbackendsmbbrowse.c:799 ../daemon/gvfsbackendsmbbrowse.c:845
msgid "The file is not a mountable"
msgstr "الملف ليس قابلا للوصل"

#: ../daemon/gvfsbackendsmbbrowse.c:867
msgid "Not a regular file"
msgstr "ليس ملفا اعتياديا"

#: ../daemon/gvfsbackendsmbbrowse.c:1112
msgid "Not a directory"
msgstr "ليس دليلا"

#: ../daemon/gvfsbackendsmbbrowse.c:1224
msgid "Windows Network Filesystem Service"
msgstr "خادوم نظام ملفات شبكة ويندوز"

#. translators: This is the name of the backend
#. Translators: this is the display name of the backend
#: ../daemon/gvfsbackendtrash.c:618 ../daemon/gvfsbackendtrash.c:1203
msgid "Trash"
msgstr "المهملات"

#: ../daemon/gvfsbackendtrash.c:993
#, c-format
msgid "%s (in trash)"
msgstr "‏%s (في المهملات)"

#: ../daemon/gvfsbackendtrash.c:1304
msgid "Can't delete trash"
msgstr "تعذّر حذف المهملات"

#: ../daemon/gvfsbackendtrash.c:1651 ../daemon/gvfsbackendtrash.c:1726
msgid "Trash directory notification not supported"
msgstr "تنبيه مجلد المهملات غير مدعوم"

#: ../daemon/gvfsdaemon.c:1032
msgid "Invalid backend type"
msgstr "نوع خلفية غير سليم"

#: ../daemon/gvfsdaemonutils.c:104
#, c-format
msgid "Error sending fd: %s"
msgstr "حدث خطأ أثناء إرسال fd: %s"

#: ../daemon/gvfsjobcloseread.c:112 ../daemon/gvfsjobclosewrite.c:120
#: ../daemon/gvfsjobcopy.c:169 ../daemon/gvfsjobcreatemonitor.c:140
#: ../daemon/gvfsjobcreatemonitor.c:151 ../daemon/gvfsjobcreatemonitor.c:174
#: ../daemon/gvfsjobcreatemonitor.c:192 ../daemon/gvfsjobdelete.c:122
#: ../daemon/gvfsjobenumerate.c:256 ../daemon/gvfsjobmakedirectory.c:122
#: ../daemon/gvfsjobmount.c:109 ../daemon/gvfsjobmountmountable.c:156
#: ../daemon/gvfsjobmove.c:168 ../daemon/gvfsjobopenforread.c:130
#: ../daemon/gvfsjobopenforwrite.c:148 ../daemon/gvfsjobopenforwrite.c:162
#: ../daemon/gvfsjobopenforwrite.c:176 ../daemon/gvfsjobqueryattributes.c:135
#: ../daemon/gvfsjobqueryfsinfo.c:131 ../daemon/gvfsjobqueryinfo.c:149
#: ../daemon/gvfsjobread.c:120 ../daemon/gvfsjobseekread.c:119
#: ../daemon/gvfsjobseekwrite.c:119 ../daemon/gvfsjobsetattribute.c:154
#: ../daemon/gvfsjobsetdisplayname.c:127 ../daemon/gvfsjobtrash.c:122
#: ../daemon/gvfsjobunmountmountable.c:132
#: ../daemon/gvfsjobunmountmountable.c:146 ../daemon/gvfsjobupload.c:169
#: ../daemon/gvfsjobwrite.c:120
msgid "Operation not supported by backend"
msgstr "لا تدعم الخلفية هذا العملية"

#: ../daemon/gvfsjobmakesymlink.c:126
msgid "Symlinks not supported by backend"
msgstr "لا تدعم الخلفية الوصلات الرمزية"

#: ../daemon/gvfsjobsetattribute.c:123
msgid "Invalid dbus message"
msgstr "رسالة dbus غير سليمة"

#: ../daemon/main.c:45
msgid "Replace old daemon."
msgstr "استبدل الخادوم القديم."

#: ../daemon/main.c:46
msgid "Don't start fuse."
msgstr "لا تبدأ fuse."

#: ../daemon/main.c:58
msgid "GVFS Daemon"
msgstr "عفريت GVFS"

#: ../daemon/main.c:61
msgid "Main daemon for GVFS"
msgstr "عفريت GVFS الرئيسي"

#. Translators: the first %s is the application name,
#. the second %s is the error message
#: ../daemon/main.c:76
#, c-format
msgid "%s: %s"
msgstr "%s: %s"

#: ../daemon/main.c:78 ../programs/gvfs-cat.c:163 ../programs/gvfs-cat.c:176
#: ../programs/gvfs-copy.c:101 ../programs/gvfs-info.c:337
#: ../programs/gvfs-ls.c:386 ../programs/gvfs-move.c:97
#: ../programs/gvfs-open.c:131 ../programs/gvfs-open.c:144
#: ../programs/gvfs-save.c:165 ../programs/gvfs-tree.c:251
#, c-format
msgid "Try \"%s --help\" for more information."
msgstr "جرّب ‪\"%s --help\"‬ لمزيد من المعلومات."

#: ../daemon/mount.c:432
msgid "Invalid arguments from spawned child"
msgstr "معطيات غير سليمة من الابن المُشعّب"

#: ../daemon/mount.c:731
#, c-format
msgid "Automount failed: %s"
msgstr "فشل الفصْل: %s"

#: ../daemon/mount.c:776
msgid "The specified location is not mounted"
msgstr "المكان المحدد ليس موصولا"

#: ../daemon/mount.c:781
msgid "The specified location is not supported"
msgstr "المكان المحدد ليس مدعوما"

#: ../daemon/mount.c:944
msgid "Location is already mounted"
msgstr "المكان موصول من قبْل"

#: ../daemon/mount.c:952
msgid "Location is not mountable"
msgstr "المكان ليس قابلا للوصْل"

#: ../hal/ghaldrive.c:142
msgid "CD-ROM"
msgstr "اسطوانة"

#: ../hal/ghaldrive.c:144
msgid "CD-R"
msgstr "قارئ اسطوانات"

#: ../hal/ghaldrive.c:146
msgid "CD-RW"
msgstr "ناسخ اسطوانات"

#: ../hal/ghaldrive.c:150
msgid "DVD-ROM"
msgstr "DVD-ROM"

#: ../hal/ghaldrive.c:152
msgid "DVD+R"
msgstr "DVD+R"

#: ../hal/ghaldrive.c:154
msgid "DVD+RW"
msgstr "DVD+RW"

#: ../hal/ghaldrive.c:156
msgid "DVD-R"
msgstr "DVD-R"

#: ../hal/ghaldrive.c:158
msgid "DVD-RW"
msgstr "DVD-RW"

#: ../hal/ghaldrive.c:160
msgid "DVD-RAM"
msgstr "DVD-RAM"

#: ../hal/ghaldrive.c:163
msgid "DVD±R"
msgstr "DVD±R"

#: ../hal/ghaldrive.c:166
msgid "DVD±RW"
msgstr "DVD±RW"

#: ../hal/ghaldrive.c:168
msgid "HDDVD"
msgstr "HDDVD"

#: ../hal/ghaldrive.c:170
msgid "HDDVD-r"
msgstr "HDDVD-r"

#: ../hal/ghaldrive.c:172
msgid "HDDVD-RW"
msgstr "HDDVD-RW"

#: ../hal/ghaldrive.c:174
msgid "Blu-ray"
msgstr "Blu-ray"

#: ../hal/ghaldrive.c:176
msgid "Blu-ray-R"
msgstr "Blu-ray-R"

#: ../hal/ghaldrive.c:178
msgid "Blu-ray-RE"
msgstr "Blu-ray-RE"

#. translators: This wis something like "CD-ROM/DVD Drive" or
#. "CD-RW/Blue-ray Drive" depending on the properties of the drive
#: ../hal/ghaldrive.c:184
#, c-format
msgid "%s/%s Drive"
msgstr "مشغّل ‪%s/%s‬"

#. translators: This wis something like "CD-ROM Drive" or "CD-RW Drive
#. depending on the properties of the drive
#: ../hal/ghaldrive.c:190
#, c-format
msgid "%s Drive"
msgstr "مشغّل %s"

#: ../hal/ghaldrive.c:194
msgid "Floppy Drive"
msgstr "مشغل أقراص مرنة"

#: ../hal/ghaldrive.c:200
msgid "Software RAID Drive"
msgstr "مُشغّل RAID برمجي"

#: ../hal/ghaldrive.c:202
msgid "USB Drive"
msgstr "مشغل USB"

#: ../hal/ghaldrive.c:204
msgid "ATA Drive"
msgstr "مشغّل ATA"

#: ../hal/ghaldrive.c:206
msgid "SCSI Drive"
msgstr "مشغّل SCSI"

#: ../hal/ghaldrive.c:208
msgid "FireWire Drive"
msgstr "مشغّل FireWire"

#: ../hal/ghaldrive.c:212
msgid "Tape Drive"
msgstr "قارئ أشرطة"

#: ../hal/ghaldrive.c:214
msgid "CompactFlash Drive"
msgstr "مشغّل فلاش مضغوط"

#: ../hal/ghaldrive.c:216
msgid "MemoryStick Drive"
msgstr "مشغل قلم ذاكرة"

#: ../hal/ghaldrive.c:218
msgid "SmartMedia Drive"
msgstr "مشغّل SmartMedia"

#: ../hal/ghaldrive.c:220
msgid "SD/MMC Drive"
msgstr "مشغّل SD/MMC"

#: ../hal/ghaldrive.c:222
msgid "Zip Drive"
msgstr "مشغّل Zip"

#: ../hal/ghaldrive.c:224
msgid "Jaz Drive"
msgstr "مشغل Jaz"

#: ../hal/ghaldrive.c:226
msgid "Thumb Drive"
msgstr "مشغل قلم"

#: ../hal/ghaldrive.c:229
msgid "Mass Storage Drive"
msgstr "مشغل تخزين عام"

#: ../hal/ghaldrive.c:728
#, c-format
msgid "Failed to eject media; one or more volumes on the media are busy."
msgstr "فشل إخراج الوسيط، واحد أو أكثر من الأجزاء على الوسيط مشغول."

#: ../hal/ghalmount.c:311 ../hal/ghalvolume.c:164
#, c-format
msgid "%.1f kB"
msgstr "%I.1f ك.بايت"

#: ../hal/ghalmount.c:316 ../hal/ghalvolume.c:169
#, c-format
msgid "%.1f MB"
msgstr "%I.1f م.بايت"

#: ../hal/ghalmount.c:321 ../hal/ghalvolume.c:174
#, c-format
msgid "%.1f GB"
msgstr "%I.1f ج.بايت"

#: ../hal/ghalmount.c:443 ../hal/ghalvolume.c:275
msgid "Mixed Audio/Data Disc"
msgstr "قرص صوت و بيانات مختلط"

#. Translators: %s is the size of the mount (e.g. 512 MB)
#. Translators: %s is the size of the volume (e.g. 512 MB)
#: ../hal/ghalmount.c:456 ../hal/ghalvolume.c:286
#, c-format
msgid "%s Media"
msgstr "وسط %s"

#. Translators: %s is the size of the volume (e.g. 512 MB)
#: ../hal/ghalvolume.c:263
#, c-format
msgid "%s Encrypted Data"
msgstr "%s بيانات معمّاة"

#: ../hal/hal-utils.c:40
msgid "CD-ROM Disc"
msgstr "اسطوانة"

#: ../hal/hal-utils.c:40
msgid "Blank CD-ROM Disc"
msgstr "اسطوانة فارغة"

#: ../hal/hal-utils.c:41
msgid "CD-R Disc"
msgstr "اسطوانة قراءة"

#: ../hal/hal-utils.c:41
msgid "Blank CD-R Disc"
msgstr "اسطوانة قراءة فارغة"

#: ../hal/hal-utils.c:42
msgid "CD-RW Disc"
msgstr "اسطوانة إعادة كتابة"

#: ../hal/hal-utils.c:42
msgid "Blank CD-RW Disc"
msgstr "اسطوانة إعادة كتابة فارغة"

#: ../hal/hal-utils.c:43 ../hal/hal-utils.c:45
msgid "DVD-ROM Disc"
msgstr "قرص DVD-ROM"

#: ../hal/hal-utils.c:43 ../hal/hal-utils.c:45
msgid "Blank DVD-ROM Disc"
msgstr "قرص DVD-RAM فارغ"

#: ../hal/hal-utils.c:44
msgid "DVD-RAM Disc"
msgstr "قرص DVD-RAM"

#: ../hal/hal-utils.c:44
msgid "Blank DVD-RAM Disc"
msgstr "قرص DVD-RAM فارغ"

#: ../hal/hal-utils.c:46
msgid "DVD-RW Disc"
msgstr "قرص DVD-RW"

#: ../hal/hal-utils.c:46
msgid "Blank DVD-RW Disc"
msgstr "قرص DVD-RW فارغ"

#: ../hal/hal-utils.c:47
msgid "DVD+R Disc"
msgstr "قرص DVD+R"

#: ../hal/hal-utils.c:47
msgid "Blank DVD+R Disc"
msgstr "قرص DVD+R فارغ"

#: ../hal/hal-utils.c:48
msgid "DVD+RW Disc"
msgstr "قرص DVD+RW فارغ"

#: ../hal/hal-utils.c:48
msgid "Blank DVD+RW Disc"
msgstr "قرص DVD+RW فارغ"

#: ../hal/hal-utils.c:49
msgid "DVD+R DL Disc"
msgstr "قرص DVD-ROM"

#: ../hal/hal-utils.c:49
msgid "Blank DVD+R DL Disc"
msgstr "قرص DVD-RAM فارغ"

#: ../hal/hal-utils.c:50
msgid "Blu-Ray Disc"
msgstr "قرص Blu-Ray"

#: ../hal/hal-utils.c:50
msgid "Blank Blu-Ray Disc"
msgstr "قرص Blu-Ray فارغ"

#: ../hal/hal-utils.c:51
msgid "Blu-Ray R Disc"
msgstr "قرص Blu-Ray R"

#: ../hal/hal-utils.c:51
msgid "Blank Blu-Ray R Disc"
msgstr "قرص Blu-Ray R فارغ"

#: ../hal/hal-utils.c:52
msgid "Blu-Ray RW Disc"
msgstr "قرص Blu-Ray RW"

#: ../hal/hal-utils.c:52
msgid "Blank Blu-Ray RW Disc"
msgstr "قرص Blu-Ray RW فارغ"

#: ../hal/hal-utils.c:53
msgid "HD DVD Disc"
msgstr "قرص HD DVD"

#: ../hal/hal-utils.c:53
msgid "Blank HD DVD Disc"
msgstr "قرص HD DVD فارغ"

#: ../hal/hal-utils.c:54
msgid "HD DVD-R Disc"
msgstr "قرص HD DVD-R"

#: ../hal/hal-utils.c:54
msgid "Blank HD DVD-R Disc"
msgstr "قرص HD DVD-R فارغ"

#: ../hal/hal-utils.c:55
msgid "HD DVD-RW Disc"
msgstr "قرص HD DVD-RW"

#: ../hal/hal-utils.c:55
msgid "Blank HD DVD-RW Disc"
msgstr "قرص HD DVD-RW فارغ"

#: ../hal/hal-utils.c:56
msgid "MO Disc"
msgstr "قرص MO"

#: ../hal/hal-utils.c:56
msgid "Blank MO Disc"
msgstr "قرص MO فارغ"

#: ../hal/hal-utils.c:57
msgid "Disc"
msgstr "قرص"

#: ../hal/hal-utils.c:57
msgid "Blank Disc"
msgstr "قرص فارغ"

#. Translators: the first %s is the program name, the second one
#. is the URI of the file, the third is the error message.
#: ../programs/gvfs-cat.c:57
#, c-format
msgid "%s: %s: error opening file: %s\n"
msgstr "‏%s: %s: خطأ في فتح الملف: %s\n"

#. Translators: the first %s is the program name, the
#. second one is the URI of the file.
#: ../programs/gvfs-cat.c:80
#, c-format
msgid "%s: %s, error writing to stdout"
msgstr "‏%s: %s، خطأ في الكتابة إلى الخرج اقياسي"

#. Translators: the first %s is the program name, the second one
#. is the URI of the file, the third is the error message.
#: ../programs/gvfs-cat.c:92
#, c-format
msgid "%s: %s: error reading: %s\n"
msgstr "‏%s: %s، خطأ في قراءة: %s\n"

#. Translators: the first %s is the program name, the second one
#. is the URI of the file, the third is the error message.
#: ../programs/gvfs-cat.c:110
#, c-format
msgid "%s: %s:error closing: %s\n"
msgstr "‏%s: %s: خطأ في غلق: %s\n"

#: ../programs/gvfs-cat.c:136
msgid "LOCATION... - concatenate LOCATIONS to standard output."
msgstr "أماكن... - جمّع الأماكن إلى الخرج القياسي"

#. Translators: this message will appear after the usage string
#. and before the list of options.
#: ../programs/gvfs-cat.c:141
msgid ""
"Concatenate files at locations and print to the standard output. Works just "
"like the traditional cat utility, but using gvfs location instead local "
"files: for example you can use something like smb://server/resource/file.txt "
"as location to concatenate."
msgstr ""
"جمّع الملفات في الأماكن واطبعهم إلى الخرج القياسي. يعمل كأدات cat التقليدية، "
"لكن باستخدام أماكن gvfs بدلا من الملفات المحلية. مثلا يمكنك استخدام شيء مثل "
"smb://server/resource/file.txt كمكان لتجميعه."

#: ../programs/gvfs-cat.c:148
msgid ""
"Note: just pipe through cat if you need its formatting option like -n, -T or "
"other."
msgstr ""
"ملاحظة: مرر إلى cat إذا كانت تريد خياراته التنسيقية مثل ‪-n‬، ‪-T‬، أو غيرها."

#. Translators: the %s is the program name. This error message
#. means the user is calling gvfs-cat without any argument.
#: ../programs/gvfs-cat.c:174 ../programs/gvfs-open.c:142
#, c-format
msgid "%s: missing locations"
msgstr "‏%s: لا أماكن"

#. Translators: the first %s is the program name, the second one
#. is the URI of the file, the third is the error message.
#: ../programs/gvfs-open.c:56
#, c-format
msgid "%s: %s: error opening location: %s\n"
msgstr "‏%s: %s: خطأ أثناء فتح المكان: %s\n"

#. Translators: the first %s is the program name, the second one
#. is the URI of the file, the third is the error message.
#: ../programs/gvfs-open.c:83
#, c-format
msgid "%s: %s: error launching application: %s\n"
msgstr "‏%s: %s: خطأ أثناء بدأ التطبيق: %s\n"

#: ../programs/gvfs-open.c:113
msgid "FILES... - open FILES with registered application."
msgstr "ملف... - افتح الملفات بالبرنامج المسجل"

#. Translators: this message will appear after the usage string
#. and before the list of options.
#: ../programs/gvfs-open.c:117
msgid ""
"Opens the file(s) with the default application registered to handle the type "
"of the file."
msgstr ""
"افتح الملف/الملفات بالبرنامج المبدئي المسجل لمعالجة هذا النوع من الملفات."

#~ msgid "File unavailable"
#~ msgstr "ملفّ غير متوفّر"
