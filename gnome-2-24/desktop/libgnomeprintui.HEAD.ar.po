# translation of libgnomeprintui.HEAD.ar.po to Arabic
# translation of libgnomeprintui.ar.po to
# translation of libgnomeprintui.po to
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) 2003 THE PACKAGE'S COPYRIGHT HOLDER
#
#
# Arafat medini <lumina@silverpen.de>, 2002.
# Arafat Medini <lumina@silverpen.de>, 2003.
# Djihed Afifi <djihed@gmail.com>, 2006.
# Khaled Hosny <khaledhosny@eglug.org>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: libgnomeprintui.HEAD.ar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-01-12 01:07+0000\n"
"PO-Revision-Date: 2008-01-11 21:19+0200\n"
"Last-Translator: Khaled Hosny <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@eglug.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n>=3 && "
"n<=10 ? 3 : n>=11 && n<=99 ? 4 : 5;\n"

#: ../libgnomeprintui/gnome-canvas-hacktext.c:128
msgid "Text"
msgstr "نص"

#: ../libgnomeprintui/gnome-canvas-hacktext.c:129
msgid "Text to render"
msgstr "نص لتصيره"

#: ../libgnomeprintui/gnome-canvas-hacktext.c:148
#: ../libgnomeprintui/gnome-canvas-hacktext.c:149
msgid "Glyphlist"
msgstr "قائمة الرموز"

#: ../libgnomeprintui/gnome-canvas-hacktext.c:155
#: ../libgnomeprintui/gnome-canvas-hacktext.c:163
msgid "Color"
msgstr "اللون"

#: ../libgnomeprintui/gnome-canvas-hacktext.c:156
msgid "Text color, as string"
msgstr "لون النص، كمقطع"

#: ../libgnomeprintui/gnome-canvas-hacktext.c:164
msgid "Text color, as an R/G/B/A combined integer"
msgstr "لون النص، كعدد صحيح مجمع لـ R/G/B/A"

#: ../libgnomeprintui/gnome-canvas-hacktext.c:171
msgid "Font"
msgstr "الخط"

#: ../libgnomeprintui/gnome-canvas-hacktext.c:172
msgid "Font as a GnomeFont struct"
msgstr "الخط كبنية GnomeFont"

#. Family frame
#: ../libgnomeprintui/gnome-font-dialog.c:164
#: ../libgnomeprintui/gnome-font-dialog.c:199
msgid "Font family"
msgstr "عائلة الخطوط"

#: ../libgnomeprintui/gnome-font-dialog.c:200
msgid "The list of font families available"
msgstr "قائمة بعائلات الخطوط المتوفرة"

#. Style frame
#: ../libgnomeprintui/gnome-font-dialog.c:209
msgid "Style"
msgstr "الأسلوب"

#: ../libgnomeprintui/gnome-font-dialog.c:250
msgid "Font style"
msgstr "أسلوب الخط"

#: ../libgnomeprintui/gnome-font-dialog.c:251
msgid "The list of styles available for the selected font family"
msgstr "قائمة الأساليب المتوفرة لعائلة الخطوط المنتقاة"

#: ../libgnomeprintui/gnome-font-dialog.c:281
msgid "Font _size:"
msgstr "_مقاس الخط:"

#: ../libgnomeprintui/gnome-font-dialog.c:773
msgid "This font does not have sample"
msgstr "ليس لهذا الخط عيّنة"

#: ../libgnomeprintui/gnome-font-dialog.c:880
msgid "Font Preview"
msgstr "معاينة الخط"

#: ../libgnomeprintui/gnome-font-dialog.c:881
msgid "Displays some example text in the selected font"
msgstr "يعرض بعض الأمثلة النصية للخط المختار"

#: ../libgnomeprintui/gnome-font-dialog.c:896
msgid "Font Selection"
msgstr "اختيار الخط"

#: ../libgnomeprintui/gnome-print-config-dialog.c:252
msgid "Default Settings"
msgstr "الإعدادات المبدئية"

#: ../libgnomeprintui/gnome-print-config-dialog.c:278
msgid "Image showing pages being printed in duplex."
msgstr "صورة تعرض الصفحات التي تُطبع مزدوجة."

#: ../libgnomeprintui/gnome-print-config-dialog.c:282
msgid "_Duplex"
msgstr "ا_زدواجا"

#: ../libgnomeprintui/gnome-print-config-dialog.c:288
msgid "Pages are printed in duplex."
msgstr "ستطبع الصفحات ازدواجا."

#: ../libgnomeprintui/gnome-print-config-dialog.c:296
msgid ""
"Image showing the second page of a duplex printed sequence to be printed "
"upside down."
msgstr ""
"صورة عارضة للصورة الثانية لمتوالية مطبوعة على نحو ازدواجي عند طبعها منكسة."

#: ../libgnomeprintui/gnome-print-config-dialog.c:301
msgid "_Tumble"
msgstr "_قلب"

#: ../libgnomeprintui/gnome-print-config-dialog.c:307
msgid ""
"If copies of the document are printed in duplex, the second page is flipped "
"upside down,"
msgstr ""
"إذا كانت ستُطبع نسخ المستند ازدواجا، فستقلب الصفحة الثانية رأسا على عقب،"

#: ../libgnomeprintui/gnome-print-config-dialog.c:327
msgid "_Printing Time:"
msgstr "وقت ال_طباعة:"

#: ../libgnomeprintui/gnome-print-config-dialog.c:353
msgid "Error while loading printer configuration"
msgstr "خطأ عند تحميل إعدادات الطابعة"

#: ../libgnomeprintui/gnome-print-copies.c:449
#: ../libgnomeprintui/gnome-print-layout-selector.c:882
#: ../libgnomeprintui/gnome-print-page-selector.c:511
msgid "Filter"
msgstr "مُرشِّح"

#: ../libgnomeprintui/gnome-print-copies.c:547
msgid "Copies"
msgstr "نُسخ"

#: ../libgnomeprintui/gnome-print-copies.c:560
msgid "N_umber of copies:"
msgstr "_عدد النُّسخ:"

#: ../libgnomeprintui/gnome-print-copies.c:580
msgid ""
"Image showing the collation sequence when multiple copies of the document "
"are printed"
msgstr "صورة تعرض تتابع الترتيب عند طبع نسخ عديدة من المستند"

#. Collate
#: ../libgnomeprintui/gnome-print-copies.c:583
msgid "C_ollate"
msgstr "_ترتيب"

#: ../libgnomeprintui/gnome-print-copies.c:588
msgid ""
"If copies of the document are printed separately, one after another, rather "
"than being interleaved"
msgstr ""
"إذا كانت ستُطبع نسخ المستند منفصلة ،فستُطبع الواحدة تلو الأخرى بدلا من تداخلها"

#. Reverse
#: ../libgnomeprintui/gnome-print-copies.c:591
msgid "_Reverse"
msgstr "م_عكوس"

#: ../libgnomeprintui/gnome-print-copies.c:594
msgid "Reverse order of pages when printing"
msgstr "تَرْتِيب عكسي للصفحات عند الطباعة"

#: ../libgnomeprintui/gnome-print-dialog.c:473
#: ../libgnomeprintui/gpaui/gpa-printer-selector.c:169
msgid "Printer"
msgstr "الطابعة"

#. Set up the dialog
#: ../libgnomeprintui/gnome-print-dialog.c:626
#: ../libgnomeprintui/gnome-print-dialog.c:683
msgid "Gnome Print Dialog"
msgstr "حوار طباعة جنوم"

#: ../libgnomeprintui/gnome-print-dialog.c:706
#: ../libgnomeprintui/gnome-print-job-preview.c:2456
msgid "Job"
msgstr "المهمة"

#: ../libgnomeprintui/gnome-print-dialog.c:722
#: ../libgnomeprintui/gnome-print-page-selector.c:590
msgid "Print Range"
msgstr "مدى الطباعة"

#: ../libgnomeprintui/gnome-print-dialog.c:750
#: ../libgnomeprintui/gnome-print-paper-selector.c:678
msgid "Paper"
msgstr "الورق"

#. Layout page
#: ../libgnomeprintui/gnome-print-dialog.c:758
msgid "Layout"
msgstr "النسق"

#: ../libgnomeprintui/gnome-print-dialog.c:799
msgid "_All"
msgstr "ال_كل"

#: ../libgnomeprintui/gnome-print-dialog.c:823
msgid "_Selection"
msgstr "الا_ختيار"

#: ../libgnomeprintui/gnome-print-dialog.c:1008
msgid "_From:"
msgstr "_من:"

#: ../libgnomeprintui/gnome-print-dialog.c:1021
msgid "Sets the start of the range of pages to be printed"
msgstr "يحدد بداية مدى الصفحات التي ستُطبع"

#: ../libgnomeprintui/gnome-print-dialog.c:1023
msgid "_To:"
msgstr "إ_لى:"

#: ../libgnomeprintui/gnome-print-dialog.c:1036
msgid "Sets the end of the range of pages to be printed"
msgstr "يحدد نهاية مدى الصفحات التي ستُطبع"

#: ../libgnomeprintui/gnome-print-job-preview.c:566
msgid "No visible output was created."
msgstr "لم ينشئ خرْج مرئي."

#: ../libgnomeprintui/gnome-print-job-preview.c:1638
msgid "all"
msgstr "الكل"

#: ../libgnomeprintui/gnome-print-job-preview.c:2448
#: ../libgnomeprintui/gnome-print-job-preview.c:2449
msgid "Number of pages horizontally"
msgstr "عدد الصفحات أفقيا"

#: ../libgnomeprintui/gnome-print-job-preview.c:2452
#: ../libgnomeprintui/gnome-print-job-preview.c:2453
msgid "Number of pages vertically"
msgstr "عدد الصفحات رأسيا"

#: ../libgnomeprintui/gnome-print-job-preview.c:2456
msgid "Print job"
msgstr "مهمة الطباعة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2551
msgid "Print"
msgstr "اطبع"

#: ../libgnomeprintui/gnome-print-job-preview.c:2551
msgid "Prints the current file"
msgstr "يطبع الملف الحالي"

#: ../libgnomeprintui/gnome-print-job-preview.c:2552
msgid "Close"
msgstr "اغلق"

#: ../libgnomeprintui/gnome-print-job-preview.c:2552
msgid "Closes print preview window"
msgstr "يغلق نافذة معاينة المطبوعة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2559
msgid "Cut"
msgstr "قص"

#: ../libgnomeprintui/gnome-print-job-preview.c:2560
msgid "Copy"
msgstr "انسخ"

#: ../libgnomeprintui/gnome-print-job-preview.c:2561
msgid "Paste"
msgstr "الصق"

#: ../libgnomeprintui/gnome-print-job-preview.c:2572
msgid "Undo"
msgstr "تراجع"

#: ../libgnomeprintui/gnome-print-job-preview.c:2572
msgid "Undo the last action"
msgstr "تراجع عن الإجراء الأخير"

#: ../libgnomeprintui/gnome-print-job-preview.c:2573
msgid "Redo"
msgstr "أعِد"

#: ../libgnomeprintui/gnome-print-job-preview.c:2573
msgid "Redo the undone action"
msgstr "أعِد الإجراء المتراجع عنه"

#: ../libgnomeprintui/gnome-print-job-preview.c:2582
msgid "First"
msgstr "الأولى"

#: ../libgnomeprintui/gnome-print-job-preview.c:2582
msgid "Show the first page"
msgstr "اظهر الصفحة الأولى"

#: ../libgnomeprintui/gnome-print-job-preview.c:2583
msgid "Previous"
msgstr "السّابقة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2583
msgid "Show previous page"
msgstr "اظهر الصفحة السابقة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2584
msgid "Next"
msgstr "التّالية"

#: ../libgnomeprintui/gnome-print-job-preview.c:2584
msgid "Show the next page"
msgstr "اظهر الصفحة التالية"

#: ../libgnomeprintui/gnome-print-job-preview.c:2585
msgid "Last"
msgstr "الأخيرة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2585
msgid "Show the last page"
msgstr "اظهر الصفحة الأخيرة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2595
#, no-c-format
msgid "100%"
msgstr "100%"

#: ../libgnomeprintui/gnome-print-job-preview.c:2595
msgid "Zoom 1:1"
msgstr "تقريب بنسبة 1 إلى 1"

#: ../libgnomeprintui/gnome-print-job-preview.c:2596
msgid "Zoom to fit"
msgstr "تقريب حتى الملاءمة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2596
msgid "Zoom to fit the whole page"
msgstr "قرّب لملائمة الصفحة بأكملها"

#: ../libgnomeprintui/gnome-print-job-preview.c:2597
msgid "Zoom in"
msgstr "قرّب"

#: ../libgnomeprintui/gnome-print-job-preview.c:2597
msgid "Zoom the page in"
msgstr "قرّب الصفحة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2598
msgid "Zoom out"
msgstr "بعّد"

#: ../libgnomeprintui/gnome-print-job-preview.c:2598
msgid "Zoom the page out"
msgstr "بعّد الصفحة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2607
msgid "Show multiple pages"
msgstr "اظهر صفحات متعددة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2608
msgid "Edit"
msgstr "حرِّر"

#: ../libgnomeprintui/gnome-print-job-preview.c:2609
msgid "Use theme"
msgstr "استخدم السِمة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2609
msgid "Use _theme colors for content"
msgstr "استخدم ألوان ال_سمة للمحتوى"

#: ../libgnomeprintui/gnome-print-job-preview.c:2632
msgid "Page Preview"
msgstr "معاينة الصفحة"

#: ../libgnomeprintui/gnome-print-job-preview.c:2633
msgid "The preview of a page in the document to be printed"
msgstr "معاينة لصفحة في المستند الذي ستتم طباعته"

#: ../libgnomeprintui/gnome-print-job-preview.c:2669
msgid "_Page: "
msgstr "_صفحة:"

#. xgettext : There are a set of labels and a GtkEntry of the form _Page: <entry> of {total pages}
#: ../libgnomeprintui/gnome-print-job-preview.c:2677
msgid "of"
msgstr "مِن"

#: ../libgnomeprintui/gnome-print-job-preview.c:2684
msgid "Page total"
msgstr "مجموع الصفحات"

#: ../libgnomeprintui/gnome-print-job-preview.c:2685
msgid "The total number of pages in the document"
msgstr "عدد الصفحات الكلي في المستند"

#: ../libgnomeprintui/gnome-print-job-preview.c:2769
msgid "Gnome Print Preview"
msgstr "معاينة جنوم للمطبوعة"

#: ../libgnomeprintui/gnome-print-layout-selector.c:858
#: ../libgnomeprintui/gnome-print-content-selector.c:119
msgid "Number of pages"
msgstr "عدد الصفحات"

#: ../libgnomeprintui/gnome-print-layout-selector.c:861
#: ../libgnomeprintui/gnome-print-layout-selector.c:862
msgid "Output width"
msgstr "عرض الخرْج"

#: ../libgnomeprintui/gnome-print-layout-selector.c:865
#: ../libgnomeprintui/gnome-print-layout-selector.c:866
msgid "Output height"
msgstr "ارتفاع الخرْج"

#: ../libgnomeprintui/gnome-print-layout-selector.c:869
#: ../libgnomeprintui/gnome-print-layout-selector.c:870
msgid "Input width"
msgstr "عرض الدخْل"

#: ../libgnomeprintui/gnome-print-layout-selector.c:873
#: ../libgnomeprintui/gnome-print-layout-selector.c:874
msgid "Input height"
msgstr "ارتفاع الدخْل"

#: ../libgnomeprintui/gnome-print-layout-selector.c:966
msgid "_Plain"
msgstr "_صِرف"

#: ../libgnomeprintui/gnome-print-layout-selector.c:976
msgid "_Handout: "
msgstr "_مكتوب:"

#: ../libgnomeprintui/gnome-print-layout-selector.c:989
msgid " pages on 1 page"
msgstr " صفحات في صفحة واحدة"

#: ../libgnomeprintui/gnome-print-layout-selector.c:996
msgid "1 page _spread on "
msgstr "صفحة واحدة _منتشرة"

#: ../libgnomeprintui/gnome-print-layout-selector.c:1009
msgid " pages"
msgstr " صفحات"

#: ../libgnomeprintui/gnome-print-layout-selector.c:1013
msgid "Leaflet, folded once and _stapled"
msgstr "نَ_شْرَة،  مثنية مرّة و ملصقة"

#: ../libgnomeprintui/gnome-print-layout-selector.c:1017
msgid "Leaflet, _folded twice"
msgstr "نَ_شْرَة,  مثنية مرتّان"

#: ../libgnomeprintui/gnome-print-page-selector.c:515
#: ../libgnomeprintui/gnome-print-content-selector.c:122
msgid "Current page"
msgstr "الصفحة الحالية"

#: ../libgnomeprintui/gnome-print-page-selector.c:518
#: ../libgnomeprintui/gnome-print-page-selector.c:519
msgid "Number of pages to select from"
msgstr "عدد الصفحات ليُختار منها"

#: ../libgnomeprintui/gnome-print-page-selector.c:522
#: ../libgnomeprintui/gnome-print-page-selector.c:523
msgid "Number of selected pages"
msgstr "عدد الصفحات المختارة"

#: ../libgnomeprintui/gnome-print-page-selector.c:605
msgid "_All pages"
msgstr "_كل الصفحات"

#: ../libgnomeprintui/gnome-print-page-selector.c:611
msgid "_Even pages"
msgstr "الصّفحات ال_زّوجيّة"

#: ../libgnomeprintui/gnome-print-page-selector.c:615
msgid "_Odd pages"
msgstr "الصفحات ال_فردية"

#: ../libgnomeprintui/gnome-print-page-selector.c:624
msgid "_Current page"
msgstr "الصفحة ال_حالية"

#: ../libgnomeprintui/gnome-print-page-selector.c:632
msgid "_Page range: "
msgstr "مدى ال_صفحات:"

#: ../libgnomeprintui/gnome-print-paper-selector.c:597
#: ../libgnomeprintui/gnome-print-paper-selector.c:741
msgid "Preview"
msgstr "معاينة"

#: ../libgnomeprintui/gnome-print-paper-selector.c:598
msgid "Preview of the page size, orientation and layout"
msgstr "معاينة عن حجم الصفحة واتجاهها و هيئتها"

#: ../libgnomeprintui/gnome-print-paper-selector.c:650
msgid "Width"
msgstr "العرض"

#: ../libgnomeprintui/gnome-print-paper-selector.c:653
msgid "Height"
msgstr "الارتفاع"

#: ../libgnomeprintui/gnome-print-paper-selector.c:656
msgid "Configuration"
msgstr "إعدادات"

#. Paper size selector
#: ../libgnomeprintui/gnome-print-paper-selector.c:695
msgid "Paper _size:"
msgstr "_مقاس الورق:"

#: ../libgnomeprintui/gnome-print-paper-selector.c:700
msgid "_Width:"
msgstr "ال_عرض:"

#: ../libgnomeprintui/gnome-print-paper-selector.c:704
msgid "_Height:"
msgstr "الا_رتفاع:"

#: ../libgnomeprintui/gnome-print-paper-selector.c:715
msgid "Metric selector"
msgstr "المنتقي المتري"

#: ../libgnomeprintui/gnome-print-paper-selector.c:717
msgid ""
"Specifies the metric to use when setting the width and height of the paper"
msgstr "يحدد القياس الذي سيستخدم أثناء ضبط عرض وارتفاع الورق"

#. Feed orientation
#: ../libgnomeprintui/gnome-print-paper-selector.c:721
msgid "_Feed orientation:"
msgstr "ا_تجاه التلقيم:"

#. Page orientation
#: ../libgnomeprintui/gnome-print-paper-selector.c:726
msgid "Page _orientation:"
msgstr "ات_جاه الصفحة:"

#. Paper source
#: ../libgnomeprintui/gnome-print-paper-selector.c:732
msgid "Paper _tray:"
msgstr "_صينية الورق:"

#: ../libgnomeprintui/gnome-print-paper-selector.c:754
msgid "Margins"
msgstr "الحواشي"

#: ../libgnomeprintui/gnome-print-paper-selector.c:763
msgid "Top"
msgstr "فوق"

#: ../libgnomeprintui/gnome-print-paper-selector.c:767
msgid "Bottom"
msgstr "تحت"

#: ../libgnomeprintui/gnome-print-paper-selector.c:771
msgid "Left"
msgstr "يسار"

#: ../libgnomeprintui/gnome-print-paper-selector.c:775
msgid "Right"
msgstr "يمين"

#. To translators: 'Print Content' can be a specific sheet, line x to y...
#: ../libgnomeprintui/gnome-print-content-selector.c:134
msgid "Print Content"
msgstr "اطبع المحتوى"

#: ../libgnomeprintui/gnome-printer-selector.c:162
msgid "Co_nfigure"
msgstr "ا_ضبط"

#: ../libgnomeprintui/gnome-printer-selector.c:171
msgid "Adjust the settings of the selected printer"
msgstr "اضبط إعدادات الطابعة المختارة"

#: ../libgnomeprintui/gnome-printer-selector.c:185
msgid "Define a new local printer"
msgstr "عرّف طابعة محلّية جديدة"

#: ../libgnomeprintui/gnome-printer-selector.c:194
msgid "_Settings:"
msgstr "الإ_عدادات:"

#: ../libgnomeprintui/gnome-printer-selector.c:203
msgid "_Location:"
msgstr "ال_موقع:"

#: ../libgnomeprintui/gpaui/gpa-option-menu.c:179
#: ../libgnomeprintui/gpaui/gpa-option-menu.c:229
msgid "No options are defined"
msgstr "لا خيارات معرفة"

#: ../libgnomeprintui/gpaui/gpa-print-to-file.c:258
msgid "Print to _file"
msgstr "اطبع ل_ملف"

#: ../libgnomeprintui/gpaui/gpa-printer-selector.c:181
msgid "State"
msgstr "الحالة"

#: ../libgnomeprintui/gpaui/gpa-printer-selector.c:188
msgid "Jobs"
msgstr "المهام"

#: ../libgnomeprintui/gpaui/gpa-printer-selector.c:195
msgid "Location"
msgstr "المكان"

#: ../libgnomeprintui/gpaui/gpa-settings-selector.c:180
msgid "No printer selected"
msgstr "لم تختر طابعة"

#: ../libgnomeprintui/gpaui/gpa-settings-selector.c:191
msgid "No settings available"
msgstr "لا إعدادات متاحة"

#: ../libgnomeprintui/gpaui/gpa-settings-selector.c:234
msgid "Add new settings"
msgstr "أضِف إعدادات جديدة"

#: ../libgnomeprintui/gpaui/gpa-spinbutton.c:315
msgid "%"
msgstr "%"

#. Percent must be first
#: ../libgnomeprintui/gpaui/gpa-spinbutton.c:316
msgid "Pt"
msgstr "نقطة"

#: ../libgnomeprintui/gpaui/gpa-spinbutton.c:317
msgid "mm"
msgstr "مم"

#: ../libgnomeprintui/gpaui/gpa-spinbutton.c:318
msgid "cm"
msgstr "سم"

#: ../libgnomeprintui/gpaui/gpa-spinbutton.c:319
msgid "m"
msgstr "م"

#: ../libgnomeprintui/gpaui/gpa-spinbutton.c:320
msgid "in"
msgstr "بوصة"

#: ../libgnomeprintui/gpaui/gpa-transport-selector.c:184
#: ../libgnomeprintui/gpaui/gpa-transport-selector.c:535
#, c-format
msgid "The specified filename \"%s\" is an existing directory."
msgstr "اسم الملف المحدد \"%s\" دليل."

#: ../libgnomeprintui/gpaui/gpa-transport-selector.c:203
#: ../libgnomeprintui/gpaui/gpa-transport-selector.c:557
#, c-format
msgid "Should the file %s be overwritten?"
msgstr "أتريد استبدال الملف %s؟"

#: ../libgnomeprintui/gpaui/gpa-transport-selector.c:248
msgid "Please specify the location and filename of the output file:"
msgstr "رجاءا حدد موقع و اسم ملف الخرْج:"

#: ../libgnomeprintui/gpaui/gpa-transport-selector.c:262
msgid "PDF Files"
msgstr "ملفات PDF"

#: ../libgnomeprintui/gpaui/gpa-transport-selector.c:267
msgid "Postscript Files"
msgstr "ملفات Postscript"

#: ../libgnomeprintui/gpaui/gpa-transport-selector.c:272
msgid "All Files"
msgstr "كل الملفات"
