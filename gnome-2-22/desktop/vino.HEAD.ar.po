# translation of vino.HEAD.ar.po to Arabic
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
#
# Ayman Hourieh <aymanh@gmail.com>, 2004.
# Djihed Afifi <djihed@gmail.com>, 2006.
# Khaled Hosny <khaledhosny@eglug.org>, 2006, 2007.
msgid ""
msgstr ""
"Project-Id-Version: vino.HEAD.ar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-21 09:02+0000\n"
"PO-Revision-Date: 2008-02-27 11:09+0100\n"
"Last-Translator: Djihed Afifi <djihed@gmail.com>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n"

#: ../capplet/vino-preferences.c:1027
#: ../capplet/vino-preferences.c:1062
#: ../capplet/vino-preferences.c:1120
#: ../server/vino-dbus-listener.c:374
#: ../server/vino-server.c:148
#, c-format
msgid "Failed to open connection to bus: %s\n"
msgstr "فشل فتح إتصال مع الناقل: %s\n"

#: ../capplet/vino-preferences.c:1329
msgid "Send this command by email"
msgstr "أرسِل هذا الأمر بالبريد الإلكتروني"

#: ../capplet/vino-preferences.c:1364
#, c-format
msgid ""
"There was an error displaying help:\n"
"%s"
msgstr ""
"حدث خطأ عند عرض المساعدة:\n"
"%s"

#: ../capplet/vino-preferences.desktop.in.in.h:1
msgid "Remote Desktop"
msgstr "سطح مكتب بعيد"

#: ../capplet/vino-preferences.desktop.in.in.h:2
msgid "Set your remote desktop access preferences"
msgstr "اضبط تفضيلات النفاذ لسطح المكتب البعيد"

#: ../capplet/vino-preferences.glade.h:1
msgid "<b>Network</b>"
msgstr "<b>شبكة</b>"

#: ../capplet/vino-preferences.glade.h:2
msgid "<b>Notification Area</b>"
msgstr "<b>مساحة التبليغ</b>"

#: ../capplet/vino-preferences.glade.h:3
msgid "<b>Security</b>"
msgstr "<b>الأمن</b>"

#: ../capplet/vino-preferences.glade.h:4
msgid "<b>Sharing</b>"
msgstr "<b>المشاركة</b>"

#: ../capplet/vino-preferences.glade.h:5
msgid "A_sk you for confirmation"
msgstr "ا_سأل للتأكيد"

#: ../capplet/vino-preferences.glade.h:6
msgid "Advanced"
msgstr "متقدم"

#: ../capplet/vino-preferences.glade.h:7
msgid "Al_ways display an icon"
msgstr "دائما ا_عرض أيقونة"

#: ../capplet/vino-preferences.glade.h:8
msgid "Allow other users to _view your desktop"
msgstr "اسمح للمستخدمين الآخرين ب_عرض سطح مكتبك"

#: ../capplet/vino-preferences.glade.h:9
msgid "General"
msgstr "عامّ"

#: ../capplet/vino-preferences.glade.h:10
msgid "If checked, remote users accessing the desktop are required to support encryption"
msgstr "إذا كان مفعلا، يجب أن يدعم المستخدون البعيدون التشفير"

#. tooltip in preferences applet
#: ../capplet/vino-preferences.glade.h:12
msgid "If checked, remote users are able to control your mouse and keyboard"
msgstr "إذا كان مفعلا، سيتمكن المستخدمون البعيدون من التحكم بالفأرة ولوحة الفاتيح"

#: ../capplet/vino-preferences.glade.h:13
msgid "If checked, screen will be locked after the last remote client disconnect"
msgstr "إذا كان مفعلا، سيتم غلق الشاشة عند اغلاق آخر مستخدم للإتصال"

#: ../capplet/vino-preferences.glade.h:14
msgid "If checked, the server will only accept connections from localhost"
msgstr "إذا كان مفعلا، سيقبل الخادم الإتصالات المحلية فقط"

#: ../capplet/vino-preferences.glade.h:15
msgid "If checked, the server will use another port, instead of the default (5900)"
msgstr "إذا كان مفعلا، سيستمع الخادم لمنفذ آخر، بدلا من الإفتراضي (5900)"

#. tooltip in preferences applet
#: ../capplet/vino-preferences.glade.h:17
msgid "If checked, your desktop will be shared"
msgstr "إذا كان مفعلا، ستتم مشاركة سطح مكتبك"

#: ../capplet/vino-preferences.glade.h:18
msgid "Remote Desktop Preferences"
msgstr "تفضيلات سطح المكتب البعيد"

#: ../capplet/vino-preferences.glade.h:19
msgid "Some of these preferences are locked down"
msgstr "بعض هذه التّفضيلات مقفل"

#: ../capplet/vino-preferences.glade.h:20
msgid "Users can view your desktop using this command:"
msgstr "يمكن للمستخدمين عرض سطح مكتبك باستخدام هذا الأمر:"

#: ../capplet/vino-preferences.glade.h:21
msgid "When a user tries to view or control your desktop:"
msgstr "عندما يحاول مستخدم عرض أو التّحكم بسطح مكتبك:"

#: ../capplet/vino-preferences.glade.h:22
msgid "_Allow other users to control your desktop"
msgstr "ا_سمح للمستخدمين الآخرين بالتّحكّم بسطح مكتبك."

#: ../capplet/vino-preferences.glade.h:23
msgid "_Lock screen on disconnect"
msgstr "ا_قفل الشاشة في حالة الغاء الإتصال"

#: ../capplet/vino-preferences.glade.h:24
msgid "_Never display an icon"
msgstr "_لاتعرض الأيقونة أبداً"

#: ../capplet/vino-preferences.glade.h:25
msgid "_Only allow local connections"
msgstr "اس_مح بالإتصالات المحلّية فقط"

#: ../capplet/vino-preferences.glade.h:26
msgid "_Only display an icon when there is someone connected"
msgstr "ا_عرض ايقونة فقط عندا يكون أحدهم متصلا"

#: ../capplet/vino-preferences.glade.h:27
msgid "_Password:"
msgstr "_كلمة السر:"

#: ../capplet/vino-preferences.glade.h:28
msgid "_Require encryption"
msgstr "التّ_شفير مطلوب:"

#: ../capplet/vino-preferences.glade.h:29
msgid "_Require the user to enter this password:"
msgstr "ا_طلب من المستخدم ادخال كلمة السر هذه:"

#: ../capplet/vino-preferences.glade.h:30
msgid "_Use an alternative port:"
msgstr "ا_ستعمل رقم منفذ بديل:"

#: ../server/vino-dbus-listener.c:188
#: ../server/vino-dbus-listener.c:218
#: ../server/vino-dbus-listener.c:249
#, c-format
msgid "Out of memory handling '%s' message"
msgstr "نفذت الذّاكرة أثناء التعامل مع الرسالة '%s' "

#: ../server/vino-dbus-listener.c:334
#, c-format
msgid "Out of memory registering object path '%s'"
msgstr "نفذت الذّاكرة أثناء تسجيل مسار الكائن '%s'"

#: ../server/vino-dbus-listener.c:407
#, c-format
msgid "Failed to acquire D-Bus name '%s'\n"
msgstr "فشل الحصول على اسم D-Bus '%s'\n"

#: ../server/vino-main.c:74
msgid "GNOME Remote Desktop"
msgstr "سطح مكتب جنوم عن بعيد"

#: ../server/vino-main.c:84
msgid "Your XServer does not support the XTest extension - remote desktop access will be view-only\n"
msgstr "لايدعم خادم X الامتداد XTest - سيكون سطح المكتب البعيد للعرض فقط\n"

#.
#. * Translators: translate "vino-mdns:showusername" to
#. * "1" if "%s's remote desktop" doesn't make sense in
#. * your language.
#.
#: ../server/vino-mdns.c:59
msgid "vino-mdns:showusername"
msgstr "0"

#.
#. * Translators: this string is used ONLY if you
#. * translated "vino-mdns:showusername" to anything
#. * other than "1"
#.
#: ../server/vino-mdns.c:71
#, c-format
msgid "%s's remote desktop on %s"
msgstr "سطح المكتب البعيد لـ %s على %s"

#: ../server/vino-prompt.c:142
msgid "Screen"
msgstr "الشّاشة"

#: ../server/vino-prompt.c:143
msgid "The screen on which to display the prompt"
msgstr "الشّاشة التي سيعرض عليها المحثّ"

#: ../server/vino-prompt.c:343
#, c-format
msgid "A user on the computer '%s' is trying to remotely view or control your desktop."
msgstr "يحاول مستخدم على الحاسوب '%s' عرض أو التّحكم بسطح مكتبك عن بعد."

#: ../server/vino-prompt.glade.h:1
msgid "<big><b>Another user is trying to view your desktop.</b></big>"
msgstr "<big><b>يحاول مستخدم آخر عرض سطح مكتبك.</b></big>"

#: ../server/vino-prompt.glade.h:2
msgid "A user on another computer is trying to remotely view or control your desktop."
msgstr "يحاول مستخدم على حاسوب آخر عرض أو التّحكم بسطح مكتبك عن بعد."

#: ../server/vino-prompt.glade.h:3
msgid "Do you want to allow them to do so?"
msgstr "أتريد السّماح لهم بفعل ذلك؟"

#: ../server/vino-prompt.glade.h:4
msgid "Question"
msgstr "السّؤال"

#: ../server/vino-prompt.glade.h:5
#: ../server/vino-util.c:63
msgid "_Allow"
msgstr "ا_سمح"

#: ../server/vino-prompt.glade.h:6
#: ../server/vino-util.c:64
msgid "_Refuse"
msgstr "ا_رفض"

#: ../server/vino-server.schemas.in.h:1
msgid "Allowed authentication methods"
msgstr "طرق التّوثّق المسموحة"

#: ../server/vino-server.schemas.in.h:2
msgid "Alternative port number"
msgstr "رقم المنفذ البديل"

#: ../server/vino-server.schemas.in.h:3
msgid "E-mail address to which the remote desktop URL should be sent"
msgstr "عنوان البريد الإلكتروني الذي سيتمّ إرسال عنوان سطح المكتب البعيد إليه"

#: ../server/vino-server.schemas.in.h:4
msgid "Enable remote desktop access"
msgstr "مكّن النفاذ لسطح المكتب عن بعد"

#: ../server/vino-server.schemas.in.h:5
msgid "If true, allows remote access to the desktop via the RFB protocol. Users on remote machines may then connect to the desktop using a vncviewer."
msgstr "إذا ضبط لـtrue، سيسمح بالوصول البعيد لسطح المكتب من خلال بروتوكول RFB.يمكن أن يتّصل المستخدمون على أجهزة بعيدة إلى سطح المكتب من خلال عارض VNC."

#: ../server/vino-server.schemas.in.h:6
msgid "If true, remote users accessing the desktop are not allowed access until the user on the host machine approves the connection. Recommended especially when access is not password protected."
msgstr "إذا ضبط لـtrue، لن يسمح للمستخدمين البعيدين بالوصول إلى سطح المكتب حتى يوافق المستخدم على الجهاز المضيف على الاتّصال. ينصح به خاصة عندما لا يكون الوصول محمياً بكلمة سر."

#: ../server/vino-server.schemas.in.h:7
msgid "If true, remote users accessing the desktop are only allowed to view the desktop. Remote users will not be able to use the mouse or keyboard."
msgstr "إذا ضبط لصحيح، لن يسمح للمستخدمين البعيدين النافذين إلى سطح المكتب بعرض سطح المكتب. لن يتمكّن المستخدمون البعيدون من استخدام الفأرة أو لوحة المفاتيح."

#: ../server/vino-server.schemas.in.h:8
msgid "If true, remote users accessing the desktop are required to support encryption. It is highly recommended that you use a client which supports encryption unless the intervening network is trusted."
msgstr "إذا ضبط لـtrue، يطلب من المستخدمين البعيدين أن يدعموا التّشفير. ينصح بشكل كبير أن تستخدم عميلاً يدعم التّشفير ما لم تكن الشبكة موثوقة."

#: ../server/vino-server.schemas.in.h:9
msgid "If true, screen will be locked after the last remote client disconnect."
msgstr "اذا كان صحيحا، سيتم غلق الشاشة عند اغلاق آخر مستخدم للإتصال."

#: ../server/vino-server.schemas.in.h:10
msgid "If true, the server will listen to another port, instead of the default (5900). The port must be specified in the 'alternative_port' key."
msgstr "لو صحيح، سيستمع الخادم لمنفذ آخر، بدلا من الإفتراضي (5900). يجب أن يحدد المنفذ قي مفتاح 'alternative_port'."

#: ../server/vino-server.schemas.in.h:11
msgid "If true, the server will only accept connections from localhost and network connections will be rejected. Set this option to true if you wish to exclusively use a tunneling mechanism to access the server, such as ssh."
msgstr "لو صحيح، سيقبل الخادم الإتصالات المحلّية فقط وسيرفض اتصلات الشبكة. اضبط هذا كصحيح، إذا أردت حصريا استعمال آليات أنفاق للنفاذ للخادم، مثل ssh."

#: ../server/vino-server.schemas.in.h:12
msgid "Listen an alternative port"
msgstr "استمع لمنفذ بديل"

#: ../server/vino-server.schemas.in.h:13
msgid "Lists the authentication methods with which remote users may access the desktop. There are two possible authentication methods; \"vnc\" causes the remote user to be prompted for a password (the password is specified by the vnc_password key) before connecting and \"none\" which allows any remote user to connect."
msgstr "يعرض قائمة بطرق التّوثّق الممكن للمستخدمين البعيدين استخدامها للوصول لسطح المكتب. هناك طريقتان ممكنتان للتّوثّق، \"vnc\" التي تسائل المستخدم البعيد عن كلمة سر (تحدّد كلمة السر بمفتاح vnc_password( قبل الاتّصال، و \"none\" التي تسمح لأي مستخدم بعيد بالاتّصال."

#: ../server/vino-server.schemas.in.h:14
msgid "Lock the screen when last user disconnect"
msgstr "اغلق الشاشة عند اغلاق آخر مستخدم للإتصال"

#: ../server/vino-server.schemas.in.h:15
msgid "Only allow local connections"
msgstr "اسمح بالإتصالات المحلّية فقط"

#: ../server/vino-server.schemas.in.h:16
msgid "Only allow remote users to view the desktop"
msgstr "اسمح للمستخدمين البعيدين بعرض سطح المكتب فقط"

#: ../server/vino-server.schemas.in.h:17
msgid "Password required for \"vnc\" authentication"
msgstr "كلمة السر مطلوبة لتوثّق \"vnc\""

#: ../server/vino-server.schemas.in.h:18
msgid "Prompt the user before completing a connection"
msgstr "اسأل المستخدم قبل إكمال اتصال"

#: ../server/vino-server.schemas.in.h:19
msgid "Require encryption"
msgstr "التّشفير مطلوب"

#: ../server/vino-server.schemas.in.h:20
msgid "The password which the remote user will be prompted for if the \"vnc\" authentication method is used. The password specified by the key is base64 encoded."
msgstr "كلمة السر التي سيُساءل المستخدم عنها إذا استخدمت طريقة توثّق \"vnc\". كلمة السر المحدّدة بالمفتاح مرمّزة بترميز قاعدة 64."

#: ../server/vino-server.schemas.in.h:21
msgid "The port which the server will listen to if the 'use_alternative_port' key is set to true. Valid values are in the range from 5000 to 50000."
msgstr "المنفذ الذي سيستمع إليه الخادم إذا ضبط مفتاح 'use_alternative_port' كصحيح. القيم المقبولة في المدى 5000 إلى 50000."

#: ../server/vino-server.schemas.in.h:22
msgid "This key controls the behavior of the status icon. There are three options: \"always\" - The icon will be always there; \"client\" - You will see the icon only when there is someone connected, this is the default behavior; \"never\" - Never shows the icon."
msgstr "يتحكم هذا المفتاح في ايقونة التنبيه. هناك ثلاث خيارات: \"always\" (دائم) \"client\" (فقط عندما يكون أحدهم متصلا - و هذا هو الإفتراضي)، \"never\"  (أبدا)"

#: ../server/vino-server.schemas.in.h:23
msgid "This key specifies the e-mail address to which the remote desktop URL should be sent if the user clicks on the URL in the Remote Desktop preferences dialog."
msgstr "يحدّد هذا المفتاح عنوان البريد الإلكتروني الذي سيتمّ إرسال عنوان سطح المكتب البعيد إليه إذا نقر المستخدم على العنوان في حوار تفضيلات سطح المكتب البعيد."

#: ../server/vino-server.schemas.in.h:24
msgid "When the status icon should be shown"
msgstr "متى يجب إظهار أيقونة التنبيه"

#: ../server/vino-shell.c:76
msgid "Remote Desktop server already running; exiting ...\n"
msgstr "خادم سطح المكتب البعيد يعمل حالياً، خروج...\n"

#: ../server/vino-shell.c:79
msgid "Problem registering the remote desktop server with bonobo-activation; exiting ...\n"
msgstr "مشكلة في تسجيل خادم سطح المكتب البعيد بتنشيط بونوبو، خروج ...\n"

#: ../server/vino-status-icon.c:102
msgid "Desktop sharing is enabled"
msgstr "مشاركة سطح المكتب مفعّلة"

#: ../server/vino-status-icon.c:110
#, c-format
msgid "One person is connected"
msgid_plural "%d people are connected"
msgstr[0] "شخص واحد متّصل"
msgstr[1] "شخصيّن متّصلين"
msgstr[2] "%d أشخاص متّصلين"
msgstr[3] "%d شخصاَ متّصلا"

#: ../server/vino-status-icon.c:245
#, c-format
msgid ""
"There was an error displaying preferences:\n"
" %s"
msgstr ""
"حدث خطأ عند عرض التفضيلات: \n"
" %s"

#: ../server/vino-status-icon.c:253
#, c-format
msgid ""
"There was an error displaying help:\n"
" %s"
msgstr ""
"حدث خطأ عند عرض المساعدة:\n"
" %s"

#: ../server/vino-status-icon.c:276
msgid ""
"Licensed under the GNU General Public License Version 2\n"
"\n"
"Vino is free software; you can redistribute it and/or\n"
"modify it under the terms of the GNU General Public License\n"
"as published by the Free Software Foundation; either version 2\n"
"of the License, or (at your option) any later version.\n"
"\n"
"Vino is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n"
"GNU General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License\n"
"along with this program; if not, write to the Free Software\n"
"Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA\n"
"02110-1301, USA.\n"
msgstr ""
"مرخّص تحت رخصة جنو العمومية العامة، نسخة 2\n"
"\n"
"فينو برنامج حر؛ بامكانك إعادة توزيعه و/أو\n"
"تعديله تحت شروط الرخصة العمومية العامة لجنو\n"
"والتي نشرتها منظمة البرمجيات الحرة؛ سواء الإصدارة 2\n"
"من الرخصة أو أي إصدارة بعدها حسب رغبتك.\n"
"\n"
"يوزّع فينو على أمل أن يكون مفيدًا لمن يستخدمه\n"
"،دون أدنى مسؤولية؛ ولا حتى أي ضمان يضمن صلاحية العرض في السوق\n"
"أو توافقه مع أي استخدام محدد.\n"
"يمكنك مراجعة الرخصة العمومية العامة لجنو لمزيد من التفاصيل.\n"
"\n"
"من المفترض أن تكون قد استلمت نسخة من رخصة جنو العامة\n"
"مع فينو؛ في حال عدم استلامك لذلك، يمكنك مكاتبة:\n"
"Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA\n"
"02111-1307  USA\n"

#. Translators comment: put your own name here to appear in the about dialog.
#: ../server/vino-status-icon.c:291
msgid "translator-credits"
msgstr ""
"فريق عربآيز للترجمة http://www.arabeyes.org :\n"
"يوسف رفه\t<yousef@raffah.com>\n"
"جهاد عفيفي\t<djihed@gmail.com>\n"
"خالد حسني\t<khaledhosny@eglug.org>"

#: ../server/vino-status-icon.c:297
msgid "Share your desktop with other users"
msgstr "شارِك سطح مكتبك مع المستخدمين الآخرين"

#: ../server/vino-status-icon.c:360
#, c-format
msgid "Are you sure you want to disconnect '%s'?"
msgstr "أمتأكّدٌ أنّك تريد قطع اتصال '%s'؟"

#: ../server/vino-status-icon.c:362
#, c-format
msgid "The remote user from '%s' will be disconnected. Are you sure?"
msgstr "سيقطع اتصال المستخدم عن بعد من '%s'. أمتأكّد؟"

#: ../server/vino-status-icon.c:367
msgid "Are you sure you want to disconnect all clients?"
msgstr "أمتأكد من أنك تريد قطع اتصّال كل العملاء؟"

#: ../server/vino-status-icon.c:368
msgid "All remote users will be disconnected. Are you sure?"
msgstr "سيقطع اتصال كل المستخدمين عن بعد. أمتأكّد؟"

#: ../server/vino-status-icon.c:380
msgid "Disconnect"
msgstr "اقطع الإتصال"

#: ../server/vino-status-icon.c:406
msgid "_Preferences"
msgstr "_تفضيلات"

#: ../server/vino-status-icon.c:421
msgid "Disconnect all"
msgstr "اقطع الكلّ"

#: ../server/vino-status-icon.c:444
#, c-format
msgid "Disconnect %s"
msgstr "ا_قطع اتصال %s"

#: ../server/vino-status-icon.c:464
msgid "_Help"
msgstr "م_ساعدة"

#: ../server/vino-status-icon.c:472
msgid "_About"
msgstr "_حوْل"

#: ../server/vino-status-icon.c:598
#, c-format
msgid "Error initializing libnotify\n"
msgstr "خطأ أثناء بدأ libnotify\n"

#: ../server/vino-status-icon.c:618
msgid "Another user is viewing your desktop"
msgstr "يشاهد مستخدم آخر سطح مكتبك"

#: ../server/vino-status-icon.c:619
#, c-format
msgid "A user on the computer '%s' is remotely viewing your desktop."
msgstr "يُشاهد مستخدم على الحاسوب '%s' سطح مكتبك عن بعد."

#: ../server/vino-status-icon.c:624
msgid "Another user is controlling your desktop"
msgstr "يتحكّم مستخدم آخر بسطح مكتبك"

#: ../server/vino-status-icon.c:625
#, c-format
msgid "A user on the computer '%s' is remotely controlling your desktop."
msgstr "تحكّم مستخدم على الحاسوب '%s' بسطح مكتبك عن بعد."

#: ../server/vino-status-icon.c:647
#, c-format
msgid "Error while displaying notification bubble: %s\n"
msgstr "خطأ أثناء عرض فقّاعة التبليغ: %s\n"

#: ../session/vino-session.c:133
msgid "Remote desktop server died, restarting\n"
msgstr "توقّف خادم سطح المكتب، إعادة التّشغيل\n"

#: ../session/vino-session.c:154
#, c-format
msgid "Activation of %s failed: %s\n"
msgstr "فشل تنشيط %s: %s\n"

#: ../session/vino-session.c:160
#, c-format
msgid "Activation of %s failed: Unknown Error\n"
msgstr "فشل تنشيط %s: خطأ مجهول\n"

#: ../session/vino-session.c:231
msgid "Failed to activate remote desktop server: tried too many times\n"
msgstr "فشل تنشيط خادم سطح المكتب البعيد، حاولت عدداً كبيراً من المرّات\n"

#: ../session/vino-session.c:295
msgid "Starting remote desktop server"
msgstr "بدء خادم سطح المكتب البعيد"

#: ../session/vino-session.c:300
msgid "Not starting remote desktop server"
msgstr "لا تبدء خادم سطح المكتب البعيد"

